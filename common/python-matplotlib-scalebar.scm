(define-module (common python-matplotlib-scalebar)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-build))

(define-public python-matplotlib-scalebar
  (package
    (name "python-matplotlib-scalebar")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "matplotlib-scalebar" version))
        (sha256
          (base32 "0pdidgp7dv6ykkspwkawpk6ibxy3xjhs0ldfmzkcay9m17qpm20l"))))
    (build-system python-build-system)
    (arguments '(#:tests? #f))
    (propagated-inputs (list python-matplotlib))
    (home-page "https://github.com/ppinard/matplotlib-scalebar")
    (synopsis "Artist for matplotlib to display a scale bar")
    (description "Artist for matplotlib to display a scale bar")
    (license license:bsd-3)))
