(define-module (common yade)
        #:use-module (guix licenses)
        #:use-module (guix packages)
        #:use-module (guix download)
      	#:use-module (guix utils)
        #:use-module (guix build utils)
        #:use-module (guix build-system cmake)
        #:use-module (guix build-system gnu)
        #:use-module (gnu packages)
        #:use-module (gnu packages pkg-config)
        #:use-module (gnu packages qt)
        #:use-module (gnu packages gl)
        #:use-module (gnu packages boost)
        #:use-module (gnu packages algebra)
        #:use-module (gnu packages gdb)
        #:use-module (gnu packages sqlite)
        #:use-module (gnu packages image-processing)
        #:use-module (gnu packages graphics)
        #:use-module (gnu packages maths)
        #:use-module (gnu packages mpi)
        #:use-module (gnu packages python)
        #:use-module (gnu packages python-xyz)
        #:use-module (gnu packages sphinx)
        #:use-module (gnu packages commencement)
        #:use-module (gnu packages graphviz)
        #:use-module (gnu packages multiprecision))

(define-public loki
(package
  (name "loki")
  (version "0.1.7")
  (source
    (origin
      (method url-fetch)
      (uri (string-append "https://downloads.sourceforge.net/project/loki-lib/Loki/Loki%200.1.7/loki-" version ".zip"))
      (sha256
        (base32 "0p23k4dipk5nnfpvhhqj9apz8rhnd62qdydjmzx8q78sxpwims4w"))))
  (build-system gnu-build-system)
  (inputs
    `(("gcc-toolchain" ,gcc-toolchain)))
  (synopsis "Loki is a C++ library of designs, containing flexible implementations of common design patterns and idioms.")
  (description
   "Loki is the name of a C++ software library written by Andrei Alexandrescu as part of his book Modern C++ Design.
    The library makes extensive use of C++ template metaprogramming and implements several commonly used tools: typelist, functor, singleton, smart pointer, object factory, visitor and multimethods.")
  (home-page "http://loki-lib.sourceforge.net/index.php?n=Main.HomePage")
  (license gpl3+)))

(define-public yade 
  (package
  (name "yade")
  (version "2021.01a")
  (source (origin
            (method url-fetch)
            (uri (string-append "https://gitlab.com/yade-dev/trunk/-/archive/2021.01a/trunk-" version ".tar.gz"))
            (sha256
             (base32
              "07fzjgyqyb8hmsxj3g76brm5si216gv174ljyxd1hpcg1qwb7yis"))))
  (build-system cmake-build-system)
  (arguments
   `(#:configure-flags
      (list "-DENABLE_POTENTIAL_BLOCKS=OFF"
	          (string-append "-DCMAKE_INSTALL_PREFIX="
			                     (assoc-ref %outputs "out"))
            (string-append "-DQGLVIEWER_INCLUDE_DIR="
                           (assoc-ref %build-inputs "libqglviewer")))
                ;;(string-append "-DQGLVIEWER_INCLUDE_DIR="
                  ;;libqglviewer))))
     #:phases
      (modify-phases %standard-phases
        (add-after 'unpack 'post-unpack
          (lambda* (#:key outputs inputs #:allow-other-keys)
	          (mkdir-p "./build")
	          (chdir "./build")))
        (replace 'configure
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
              (apply invoke "cmake" "../" configure-flags)))))
      #:tests? #f))
	        ;;(lambda _ (invoke
	        ;;"cmake" "../trunk"
	        ;;configure-flags))))))
  (propagated-inputs
    `(("gdb" ,gdb)
      ("sqlite" ,sqlite)
      ("openmpi" ,openmpi)
      ("python" ,python)
      ("python-numpy" ,python-numpy)
      ("python-matplotlib" ,python-matplotlib)
      ("python-sphinx" ,python-sphinx)
      ("python-ipython" ,python-ipython)
      ("python-mpi4py" ,python-mpi4py)
      ("python-mpmath" ,python-mpmath)
      ("python-pygraphviz" ,python-pygraphviz)
      ("python-xlib" ,python-xlib)
      ("python-future" ,python-future)
      ("python-pyqt" ,python-pyqt)
      ("python-pyqtwebengine" ,python-pyqtwebengine)
      ("python-qtpy" ,python-qtpy))
  )
  (inputs
    `(("boost" ,boost)
      ("qtbase" ,qtbase)
      ;;("qt" ,qt)
      ("freeglut" ,freeglut)
      ("gts" ,gts)
      ("pkg-config" ,pkg-config)
      ("libqglviewer" ,libqglviewer)
      ("eigen" ,eigen)
      ;;("loki" ,loki)
      ;;("vtk" ,vtk)
      ;; Plus dispo sur le channel guix
      ;;("qtwebkit" ,qtwebkit)
      ("openblas" ,openblas)
      ("suitesparse" ,suitesparse)
      ("gcc-toolchain" ,gcc-toolchain)
      ("metis" ,metis)
      ("cgal" ,cgal)
      ("qtsvg" ,qtsvg)
      ("qtwebengine" ,qtwebengine)
      ("mpfr" ,mpfr))
  )

  (synopsis "Yade is an extensible open-source framework for discrete numerical models, focused on Discrete Element Method.")
  (description
   "Yade is an extensible open-source framework for discrete numerical models, focused on Discrete Element Method. The computation parts are written in c++ using flexible object model, allowing independent implementation of new algorithms and interfaces. Python is used for rapid and concise scene construction, simulation control, postprocessing and debugging.")
  (home-page "https://www.yade-dem.org/")
  (license gpl3+)))