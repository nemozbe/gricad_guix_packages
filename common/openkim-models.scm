(define-module (common openkim-models)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (guix utils)
      #:use-module (guix build-system cmake)
      #:use-module (gnu packages)
      #:use-module (gnu packages base)
      #:use-module (gnu packages compression)
      #:use-module (gnu packages pkg-config)
      #:use-module (gnu packages commencement))



(define-public openkim-models
  (package
    (name "openkim-models")
    (version "2021-08-11")
    (source (origin
	      (method url-fetch)
	      (uri (string-append "https://s3.openkim.org/archives/collection/openkim-models-" version ".txz"))
	      (sha256
		(base32
		  "1nknwm66avl6ahhczxc55p6j94xw51axsfw277c9fwkqd4cj8bgl"))))
    (build-system cmake-build-system)
    (arguments
      `(#:configure-flags
        (list
          "-DCMAKE_BUILD_TYPE=Release"
          (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out")))
        #:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found
          (delete 'fix-and-disable-failing-tests)
          (delete 'configure)
          (delete 'build)
          (delete 'sanity-check)
          (delete 'check)
          (delete 'validate-runpath)
          (add-after 'unpack 'post-unpack
            (lambda* (#:key outputs inputs #:allow-other-keys)
              (invoke "tar" "xf" (string-append "openkim-models-" ,version ".tar"))
            )
          )
	  (replace 'install
              (lambda* (#:key outputs #:allow-other-keys)
                  (let* ((out (assoc-ref outputs "out")))
                  (copy-recursively "." out))))
        )
        #:tests? #f
       )
    )
    (inputs
      `(("tar" ,tar)
        ("pkg-config" ,pkg-config)))
    (synopsis "Open Knowledgebase of Interatomic Models")
    (description "OpenKIM is a curated repository of interatomic potentials and analytics for making classical molecular simulations of materials reliable, reproducible, and accessible.")
    (home-page "https://openkim.org/")
    (license license:lgpl2.1+)))