(define-module (common nco)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gcc)
  #:use-module ((gnu packages base) #:prefix base:)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages java)
  #:use-module (gnu packages c)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages commencement))

(define-public nco
  (package
    (name "nco")
    (version "4.9.8")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/nco/nco/archive/" version ".tar.gz"))
        (sha256
          (base32
            "0jzfcq6nv9hsmsakmqyhljizf0nhnwq4ihby24xyq744y23yiwqy"))))
    (build-system gnu-build-system)
    (inputs
      `(("antlr2" ,antlr2)
  	    ("curl" ,curl)
  	    ("gsl" ,gsl)
  	    ("netcdf" ,netcdf)
  	    ("netcdf-fortran" ,netcdf-fortran)
  	    ("udunits" ,udunits)
  	    ("expat" ,expat)))
    (native-inputs
      `(("autoconf" ,autoconf)
  	    ("automake" ,automake)
  	    ("bison" ,bison)
  	    ("flex" ,flex)
  	    ("which" ,base:which)
  	    ("libtool" ,libtool)))
    (home-page "http://nco.sourceforge.net/#Source")
    (synopsis
      "NetCDF Operator toolkit")
    (description
      "The NCO (netCDF Operator) toolkit manipulates and analyzes data stored 
      in netCDF-accessible formats, including DAP, HDF4, and HDF5")
    (license license:gpl3)))