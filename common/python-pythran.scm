(define-module (common python-pythran)
   #:use-module ((guix licenses) #:prefix license:)
   #:use-module (gnu packages)
   #:use-module (guix packages)
   #:use-module (guix utils)
   #:use-module (guix download)
   #:use-module (guix build-system python)
   #:use-module (gnu packages python) 
   #:use-module (gnu packages python-xyz))

(define-public python-pythran
  (package
    (name "python-pythran")
    (version "0.12.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "pythran" version))
              (sha256
               (base32
                "17qg0q3r5krj34703ldinpgvrs95anc84kyiy1r77rgb7q6xvwzg"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs (list python-beniget python-gast python-numpy
                             python-ply))
    (home-page "https://github.com/serge-sans-paille/pythran")
    (synopsis "Ahead of Time compiler for numeric kernels")
    (description "Ahead of Time compiler for numeric kernels")
    (license #f)))
