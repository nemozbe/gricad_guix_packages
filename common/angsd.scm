(define-module (common angsd)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (guix utils)
      #:use-module (guix build utils)
      #:use-module (guix build-system gnu)
      #:use-module (gnu packages)
      #:use-module (gnu packages curl)
      #:use-module (gnu packages compression)
      #:use-module (gnu packages tls)
      #:use-module (gnu packages python)
      #:use-module (gnu packages pkg-config)
      #:use-module (gnu packages gcc)
      #:use-module (gnu packages bioinformatics)
      #:use-module (gnu packages boost)
      #:use-module (gnu packages xml)
      #:use-module (gnu packages maths)
      #:use-module (gnu packages commencement)
      #:use-module (gnu packages python))

(define-public htslib-1.15
  (package/inherit htslib
    (version "1.15")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/samtools/htslib/releases/download/"
                    version "/htslib-" version ".tar.bz2"))
              (sha256
               (base32
                "1n9jspki4756bb58kyazgd3fgdvzp2ljxj3wh5b2z8h32n8lk7qs"))))))

(define-public angsd
  (package
    (name "angsd")
    (version "0.937")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/ANGSD/angsd/archive/refs/tags/" version ".tar.gz"))
        (sha256
          (base32
            "0mnxnf2a7w0fnsdjvwf9snz5a1nkgmx31ra6763rlcmsq0xv64a7"))))
    (build-system gnu-build-system)
    (arguments
     `(#:make-flags
        (list "HTSSRC=systemwide"
              "CC=gcc"
  	          (string-append "prefix="
  			                     (assoc-ref %outputs "out")))
       #:phases
        (modify-phases %standard-phases
          (delete 'check)
          (delete 'configure))))
    (inputs
      `(("curl" ,curl)
        ("htslib" ,htslib-1.15)
        ("bzip2" ,bzip2)
        ("python" ,python-wrapper)
        ("zlib" ,zlib)
        ("openssl" ,openssl)
        ("xz" ,xz)))
    (home-page "http://www.popgen.dk/angsd")
    (synopsis
      "Program for analysing NGS data")
    (description
      "Program for analysing NGS data")
    (license license:gpl2)))