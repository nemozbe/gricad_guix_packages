(define-module (common hpdbscan)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (guix git-download)
      #:use-module (guix utils)
      #:use-module (guix build utils)
      #:use-module (guix build-system cmake)
      #:use-module (gnu packages)
      #:use-module (gnu packages swig)
      #:use-module (gnu packages pkg-config)
      #:use-module (gnu packages gcc)
      #:use-module (gnu packages boost)
      #:use-module (gnu packages xml)
      #:use-module (gnu packages maths)
      #:use-module (gnu packages mpi)
      #:use-module (gnu packages commencement)
      #:use-module (gnu packages python-xyz)
      #:use-module (gnu packages python))


(define-public hpdbscan
  (let ((branch "master")
        (commit "ae0c74cd2dd308529f0d3c19ab81280a73a7cabb"))
  (package
    (name "hpdbscan")
    (version (git-version "1" branch commit))
    (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/Markus-Goetz/hpdbscan")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "01f8z2adiyxl4rwlis22f75c718b88fx3flg0aqlrlsmrly14ggx"))))
    (build-system cmake-build-system)
    (arguments
     `(#:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
          (delete 'fix-and-disable-failing-tests)
          (delete 'sanity-check)
          (delete 'check)
          (replace 'configure
            (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
  	          (mkdir-p "./build")
  	          (chdir "./build")
                    (apply invoke "cmake" ".." configure-flags))) 
          (replace 'install
             (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                    (bindir (string-append out "/bin"))
                    (libdir (string-append out "/lib")))
                    (mkdir-p bindir)
                    (mkdir-p libdir)
                    (copy-file "hpdbscan" (string-append bindir "/hpdbscan"))
                    (copy-file "libhpdbscan.so" (string-append libdir "/libhpdbscan.so"))))))))
    (propagated-inputs
      `(("python" ,python-wrapper)
        ("openmpi" ,openmpi)
        ("swig" ,swig)
        ("python-mpi4py" ,python-mpi4py)
        ("hdf5" ,hdf5-1.8)))
    (home-page "https://github.com/Markus-Goetz/hpdbscan")
    (synopsis
      "Highly parallel DBSCAN (HPDBSCAN) is a shared- and distributed-memory parallel implementation of the Density-Based Spatial Clustering for Applications with Noise (DBSCAN) algorithm.")
    (description
      "Highly parallel DBSCAN (HPDBSCAN) is a shared- and distributed-memory parallel implementation of the Density-Based Spatial Clustering for Applications with Noise (DBSCAN) algorithm. It is written in C++ and may be used as shared library and command line tool.")
    (license license:expat))))