(define-module (common python-packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (common gmt)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages astronomy)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages check)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages time)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages compression)
  ;;#:use-module (past packages python)
  #:use-module (gnu packages geo))

(define-public python-pyproj
  (package
    (name "python-pyproj")
    (version "2.6.1.post1")
    (license license:gpl3+)
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pyproj" version))
        (sha256
          (base32
            "07j7zazffsmck0mmfx9aqxdjnjpfm1sv4df6jw1n2hdxmfs04nsg"))))
    (build-system python-build-system)
    ;; Delete check phase to pass the following error
    ;; -c: error: unrecognized arguments: test
    ;; Cross your fingers...
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (delete 'check))))
    (native-inputs
      `(("python-cython" ,python-cython)))
    (propagated-inputs
      `(("proj" ,proj)))
    (home-page "https://github.com/pyproj4/pyproj")
    (synopsis
      "Python interface to PROJ (cartographic projections and coordinate transformations library)")
    (description
      "Python interface to PROJ (cartographic projections and coordinate transformations library)")
))

(define-public python-pyimgur
  (package
    (name "python-pyimgur")
    (version "0.6.0")
    (license license:gpl3+)
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pyimgur" version))
        (sha256
          (base32
            "0i4irdyq3jyig8b2v78m3lik5vc75jv0sf512jndjfv62izwlxpp"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-requests" ,python-requests)))
    (home-page "https://github.com/Damgaard/PyImgur")
    (synopsis "The easy way of using Imgur.")
    (description "The easy way of using Imgur.")
))

(define-public python-obspy
  (package
    (name "python-obspy")
    (version "1.2.2")
    (license license:gpl3+)
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "obspy" version ".zip"))
        (sha256
          (base32
            "17j1vidxyp0bfsf90jfszw4nnk8vm9c07yk34mv9gdgfbf8v1wm0"))))
    (build-system python-build-system)
    ;;; Remove check phase (again) because of  that error : 
    ;;; Distribution.__init__(self, attrs)
    ;;; error: [Errno -2] Name or service not known
    ;;; s.connect(( geofon.gfz-potsdam.de 18000 ))
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (delete 'check))))
    (propagated-inputs
      `(("python-decorator" ,python-decorator)
        ("python-future" ,python-future)
        ("python-lxml" ,python-lxml)
        ("python-matplotlib" ,python-matplotlib)
        ("python-numpy" ,python-numpy)
        ("python-requests" ,python-requests)
        ("python-scipy" ,python-scipy)
        ("python-setuptools" ,python-setuptools)
        ("python-sqlalchemy" ,python-sqlalchemy)))
    (native-inputs
      `(("unzip" ,unzip)
        ("python-flake8" ,python-flake8)
        ("python-mock" ,python-mock)
        ("python-pep8-naming" ,python-pep8-naming)
        ("python-pyimgur" ,python-pyimgur)
        ("python-cython" ,python-cython)
        ("python-pyproj" ,python-pyproj)))
    (home-page "https://www.obspy.org")
    (synopsis
      "ObsPy - a Python framework for seismological observatories.")
    (description
      "ObsPy - a Python framework for seismological observatories.")
))

(define-public python-h5py-mpi
    (package
        (name "python-h5py-mpi")
        (version "2.10.0")
        (source
         (origin
          (method url-fetch)
          (uri (pypi-uri "h5py" version))
          (sha256
           (base32
            "0baipzv8n93m0dq0riyi8rfhzrjrfrfh8zqhszzp1j2xjac2fhc4"))))
        (build-system python-build-system)
        (arguments
        `(#:tests? #f ; no test target
          #:phases
           (modify-phases %standard-phases
             (add-after 'unpack 'fix-hdf5-paths
              (lambda* (#:key inputs #:allow-other-keys)
         	    (let ((hdf5 (assoc-ref inputs "hdf5-parallel-openmpi")))
         	    (setenv "CC" "mpicc")
         	    (setenv "HDF5_MPI" "ON")
         	    (setenv "HDF5_DIR" hdf5) 
                  (substitute* "setup_build.py"
                    (("\\['/opt/local/lib', '/usr/local/lib'\\]")
                     (string-append "['" hdf5 "/lib" "']"))
                    (("'/opt/local/include', '/usr/local/include'")
                     (string-append "'" hdf5 "/include" "'")))
                  (substitute* "setup_configure.py"
                    (("\\['/usr/local/lib', '/opt/local/lib'\\]")
                     (string-append "['" hdf5 "/lib" "']")))
                  #t)
	        )))))
        (propagated-inputs
         `(("python-six" ,python-six)
           ("python-mpi4py" ,python-mpi4py-3.1)
           ("openssh" ,openssh)
           ("python-numpy" ,python-numpy)))
        (inputs
           `(("hdf5-parallel-openmpi" ,hdf5-parallel-openmpi)
	         ("openmpi" ,openmpi)
	         ("python-cython" ,python-cython)))
        (native-inputs
         `(("python-pkgconfig" ,python-pkgconfig)
           ("pkg-config" ,pkg-config)))
        (home-page "https://www.h5py.org/")
        (synopsis "Read and write HDF5 files from Python")
        (description
         "The h5py package provides both a high- and low-level interface to the HDF5     library from Python.  The low-level interface is intended to be a complete wrapping of the HDF5 API, while the high-level component supports access to HDF5 files, datasets and groups using established Python and NumPy
         concepts.")
        (license license:bsd-3)))

;;; Stingray packages

(define-public python-jplephem
  (package
    (name "python-jplephem")
    (version "2.15")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "jplephem" version))
        (sha256
          (base32
            "1ca3dswsslij79qg6dcijjz4l0fj6nzmxld8z93v45ahlkhps0g0"))))
    (build-system python-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (delete 'check))))
    (propagated-inputs
      `(("python-numpy" ,python-numpy)))
        ;("python-setuptools" ,python-setuptools)))
    (home-page "")
    (synopsis
      "Use a JPL ephemeris to predict planet positions.")
    (description
      "Use a JPL ephemeris to predict planet positions.")
    (license license:expat)))

(define-public python-skyfield
  (package
    (name "python-skyfield")
    (version "1.35")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "skyfield" version))
        (sha256
          (base32
            "1ff1nr6bh2sag1nzahjpfpzwfv55y57rykblw4mx8snrkjxjkcm5"))))
    (build-system python-build-system)
    (arguments 
        '(#:tests? #f))
    (native-inputs
      `(("python-sgp4" ,python-sgp4)
        ("python-certifi" ,python-certifi)
        ("python-jplephem" ,python-jplephem)))
    (home-page
      "http://github.com/brandon-rhodes/python-skyfield/")
    (synopsis "Elegant astronomy for Python")
    (description "Elegant astronomy for Python")
    (license license:expat)))

; (define-public python-pytest-astropy-header
;   (package
;     (name "python-pytest-astropy-header")
;     (version "0.1.2")
;     (source
;       (origin
;         (method url-fetch)
;         (uri (pypi-uri "pytest-astropy-header" version))
;         (sha256
;           (base32
;             "1y87agr324p6x5gvhziymxjlw54pyn4gqnd49papbl941djpkp5g"))))
;     (build-system python-build-system)
;     (propagated-inputs
;       `(("python-pytest" ,python-pytest)))
;     (native-inputs
;       `(("python-astropy" ,python-astropy)
;         ("python-codecov" ,python-codecov)
;         ("python-coverage" ,python-coverage)
;         ("python-numpy" ,python-numpy)
;         ("python-pytest" ,python-pytest)
;         ("python-pytest-cov" ,python-pytest-cov)))
;     (home-page "http://astropy.org")
;     (synopsis
;       "pytest plugin to add diagnostic information to the header of the test output")
;     (description
;       "pytest plugin to add diagnostic information to the header of the test output")
;     (license #f)))

(define-public python-pytest-astropy
  (package
    (name "python-pytest-astropy")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pytest-astropy" version))
        (sha256
          (base32
            "18j6z6y2fvykmcs5z0mldhhaxxn6wzpnhlm2ps7m8r5z5kmh1631"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-hypothesis" ,python-hypothesis)
        ("python-pytest" ,python-pytest)
        ("python-pytest-arraydiff"
         ,python-pytest-arraydiff)
        ; ("python-pytest-astropy-header"
        ;  ,python-pytest-astropy-header)
        ("python-pytest-cov" ,python-pytest-cov)
        ("python-setuptool-scm" ,python-setuptools-scm)
        ("python-pytest-doctestplus"
         ,python-pytest-doctestplus)
        ("python-pytest-filter-subpackage"
         ,python-pytest-filter-subpackage)
        ("python-pytest-openfiles"
         ,python-pytest-openfiles)
        ("python-pytest-remotedata"
         ,python-pytest-remotedata)))
    (home-page
      "https://github.com/astropy/pytest-astropy")
    (synopsis
      "Meta-package containing dependencies for testing")
    (description
      "Meta-package containing dependencies for testing")
    (license license:bsd-3)))

(define-public python-pyerfa
  (package
    (name "python-pyerfa")
    (version "1.7.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pyerfa" version))
        (sha256
          (base32
            "09i2qcsvxd3q04a5yaf6fwzg79paaslpksinan9d8smj7viql15i"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-numpy" ,python-numpy)))
    (native-inputs
      `(("python-pytest" ,python-pytest)
        ("python-setuptool-scm" ,python-setuptools-scm)
        ("python-pytest-doctestplus"
         ,python-pytest-doctestplus)))
    (home-page "https://github.com/liberfa/pyerfa")
    (synopsis "Python bindings for ERFA")
    (description "Python bindings for ERFA")
    (license #f)))

(define-public python-extension-helpers
  (package
    (name "python-extension-helpers")
    (version "0.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "extension-helpers" version))
        (sha256
          (base32
            "10iqjzmya2h4sk765dlm1pbqypwlqyh8rw59a5m9i63d3klnz2mc"))
        (patches (search-patches
                  "extensions-helper.patch"))))
    (build-system python-build-system)
    (arguments 
        '(#:tests? #f))
    (native-inputs
      `(("python-coverage" ,python-coverage)
        ;("python-pytest-astropy" ,python-pytest-astropy)
        ("python-pytest-cov" ,python-pytest-cov)))
    (home-page
      "https://github.com/astropy/astropy-helpers")
    (synopsis
      "Utilities for building and installing packages in the Astropy ecosystem")
    (description
      "Utilities for building and installing packages in the Astropy ecosystem")
    (license #f)))

(define-public python-astropy
  (package
    (name "python-astropy")
    (version "4.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "astropy" version))
        (sha256
          (base32
            "0snw1abrwaqkziw2vambbi9g0jgah5la04x4ckg9k0wv8a54y69c"))))
    (build-system python-build-system)
    (arguments 
        '(#:tests? #f))
    (propagated-inputs
      `(("python-numpy" ,python-numpy)
        ("python-extension-helpers" ,python-extension-helpers)
        ("python-pyerfa" ,python-pyerfa)))
    (native-inputs
      `(("python-coverage" ,python-coverage)
        ("python-ipython" ,python-ipython)
        ("python-objgraph" ,python-objgraph)
        ("python-setuptool-scm" ,python-setuptools-scm)
        ("python-cython" ,python-cython)
        ; ("python-pytest-astropy" ,python-pytest-astropy)
        ("python-pytest-xdist" ,python-pytest-xdist)
        ("python-sgp4" ,python-sgp4)
        ("python-skyfield" ,python-skyfield)))
    (home-page "http://astropy.org")
    (synopsis
      "Astronomy and astrophysics core library")
    (description
      "Astronomy and astrophysics core library")
    (license #f)))

(define-public python-pyfftw
  (package
    (name "python-pyfftw")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pyFFTW" version))
        (sha256
          (base32
            "1c4clnhghd25m89gi4x2fag6kqqxmvdqi7fpdyi0hn577j18x630"))))
    (build-system python-build-system)
    (arguments 
        '(#:tests? #f))
    (propagated-inputs
      `(("python-numpy" ,python-numpy)
        ("fftw" ,fftw)
        ("python-cython" ,python-cython)))
    (home-page "https://github.com/pyFFTW/pyFFTW")
    (synopsis
      "A pythonic wrapper around FFTW, the FFT library, presenting a unified interface for all the supported transforms.")
    (description
      "A pythonic wrapper around FFTW, the FFT library, presenting a unified interface for all the supported transforms.")
    (license #f)))

(define-public python-stingray
  (package
    (name "python-stingray")
    (version "0.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "stingray" version))
        (sha256
          (base32
            "0j7l1qvgfwhmc8wgis9a5g07sz2vwn8wadgh3hr7aiv42i6f0v2q"))))
    (build-system python-build-system)
    (arguments 
        '(#:tests? #f))
    (propagated-inputs
      `(("python-astropy" ,python-astropy)
        ("python-matplotlib" ,python-matplotlib)
        ("python-numba" ,python-numba)
        ("python-pyfftw" ,python-pyfftw)
        ("python-setuptool-scm" ,python-setuptools-scm)
        ("python-numpy" ,python-numpy)
        ("python-scipy" ,python-scipy)
        ("python-six" ,python-six)))
    ;(native-inputs
    ;  `(("python-pytest-astropy" ,python-pytest-astropy)))
    (home-page "https://stingray.readthedocs.io/")
    (synopsis
      "Time Series Methods For Astronomical X-ray Data")
    (description
      "Time Series Methods For Astronomical X-ray Data")
    (license license:expat)))

(define-public python-lic
  (package
    (name "python-lic")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "lic" version))
        (sha256
          (base32
            "153x80cc1zqmpv6n69s4qg7db2ivkpy5v2a20mvlk8fig0x35g2a"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-tqdm" ,python-tqdm-4.47)
        ("python-imageio" ,python-imageio-up)))
    (arguments
          `(#:tests? #f))
    (home-page "https://gitlab.com/szs/lic/")
    (synopsis
      "Line integral convolution for numpy arrays")
    (description
      "Line integral convolution for numpy arrays")
    (license license:expat)))

(define-public python-rich
  (package
    (name "python-rich")
    (version "10.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "rich" version))
        (sha256
          (base32
            "1sf9wb3zk3iq6w1claxi6z79ihqx8by7qsa10g2qh630j48461cg"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-typing-extensions" ,python-typing-extensions)
        ("python-pygments" ,python-pygments)
        ("python-colorama" ,python-colorama)
        ("python-commonmark" ,python-commonmark)))
    (arguments
          `(#:tests? #f))
    (home-page "https://github.com/willmcgugan/rich")
    (synopsis
      "Render rich text, tables, progress bars, syntax highlighting, markdown and more to the terminal")
    (description
      "Render rich text, tables, progress bars, syntax highlighting, markdown and more to the terminal")
    (license license:expat)))

(define-public python-inifix
  (package
    (name "python-inifix")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "inifix" version))
        (sha256
          (base32
            "0zipp8ymxfgi2vzgicaahcl3x6gnkn8zx04f7bjzxa3hsmzy50dm"))))
    (build-system python-build-system)
    (arguments
          `(#:tests? #f))
    (propagated-inputs
      `(("python-more-itertools" ,python-more-itertools-8.4)))
    (native-inputs
      `(
        ;("python-pre-commit" ,python-pre-commit)
        ("python-pytest" ,python-pytest)))
    (home-page
      "https://github.com/neutrinoceros/inifix")
    (synopsis
      "I/O facility for Idefix/Pluto configuration files")
    (description
      "I/O facility for Idefix/Pluto configuration files")
    (license #f)))

(define-public python-nonos
  (package
    (name "python-nonos")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "nonos" version))
        (sha256
          (base32
            "132333dmbnpiiq5h5ph0f2bjh6anpmzv086q9jlmx39sz7701fbk"))))
    (build-system python-build-system)
    (arguments
          `(#:tests? #f))
    (propagated-inputs
      `(("python-inifix" ,python-inifix)
        ("python-lic" ,python-lic)
        ("python-matplotlib" ,python-matplotlib-3.2)
        ("python-numpy" ,python-numpy-1.18)
        ("python-rich" ,python-rich)
        ("python-toml" ,python-toml)))
    (home-page "https://github.com/volodia99/nonos")
    (synopsis
      "A tool to analyze results from idefix/pluto simulations (for protoplanetary disks more specifically)")
    (description
      "A tool to analyze results from idefix/pluto simulations (for protoplanetary disks more specifically)")
    (license #f)))

(define-public python-matplotlib-3.2
  (package (inherit python-matplotlib)
    (version "3.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "matplotlib" version))
       (sha256
        (base32
         "1visv738jq9pkp51dpcf1dq7c6fg3qkl4bz82k26y251rv6zkqpz"))))
    (propagated-inputs ; the following packages are all needed at run time
    `(("python-cycler" ,python-cycler)
      ("python-kiwisolver" ,python-kiwisolver)
      ("python-pyparsing" ,python-pyparsing)
      ("python-pygobject" ,python-pygobject)
      ("gobject-introspection" ,gobject-introspection)
      ("python-tkinter" ,python "tk")
      ("python-dateutil" ,python-dateutil)
      ("python-numpy" ,python-numpy-1.18)
      ("python-pillow" ,python-pillow)
      ("python-pytz" ,python-pytz)
      ("python-six" ,python-six)
       ;; From version 1.4.0 'matplotlib' makes use of 'cairocffi' instead of
       ;; 'pycairo'. However, 'pygobject' makes use of a 'pycairo' 'context'
       ;; object. For this reason we need to import both libraries.
       ;; https://cairocffi.readthedocs.io/en/stable/cffi_api.html#converting-pycairo-wrappers-to-cairocffi
      ("python-pycairo" ,python-pycairo)
      ("python-cairocffi" ,python-cairocffi)))))

(define-public python-matplotlib-3.0.1
  (package (inherit python-matplotlib)
    (version "3.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "matplotlib" version))
       (sha256
        (base32
         "1pa4frv4jg41g00zqshxhl56k7y4kyjmpynhm8bpcb5ca0n7iy3h"))))
    (arguments
        `(#:tests? #f
          #:phases
            (modify-phases %standard-phases
              (delete 'fix-and-disable-failing-tests)
              (delete 'sanity-check))))
    (propagated-inputs ; the following packages are all needed at run time
    `(("python-cycler" ,python-cycler)
      ("python-kiwisolver" ,python-kiwisolver)
      ("python-pyparsing" ,python-pyparsing)
      ("python-pygobject" ,python-pygobject)
      ("gobject-introspection" ,gobject-introspection)
      ("python-tkinter" ,python "tk")
      ("python-dateutil" ,python-dateutil)
      ("python-numpy" ,python-numpy)
      ("python-pillow" ,python-pillow)
      ("python-pytz" ,python-pytz)
      ("python-six" ,python-six)
       ;; From version 1.4.0 'matplotlib' makes use of 'cairocffi' instead of
       ;; 'pycairo'. However, 'pygobject' makes use of a 'pycairo' 'context'
       ;; object. For this reason we need to import both libraries.
       ;; https://cairocffi.readthedocs.io/en/stable/cffi_api.html#converting-pycairo-wrappers-to-cairocffi
      ("python-pycairo" ,python-pycairo)
      ("python-cairocffi" ,python-cairocffi)))))

(define-public python-numpy-1.18
  (package (inherit python-numpy)
    (version "1.18.5")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/numpy/numpy/releases/download/v"
             version "/numpy-" version ".tar.gz"))
       (sha256
        (base32
         "07z5m1jbs6lnr1pqs3kdkl7cs4vgsrffzdp8xv66c299qp8mn29c"))))))

(define-public python-tqdm-4.47
  (package (inherit python-tqdm)
    (version "4.47.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "tqdm" version))
         (sha256
           (base32
             "1dx7iccv7qpdx72m1jynwcgmz3ixddb6fj3fngb817xk7rnpmvv3"))))
      (arguments
          `(#:tests? #f))))

(define-public python-imageio-up
  (package (inherit python-imageio)
    (name "python-imageio-up")
    (propagated-inputs
     `(("python-numpy" ,python-numpy-1.18)
       ("python-pillow" ,python-pillow)
       ("python-psutil" ,python-psutil)))))

(define-public python-mpi4py-3.1
  (package
    (name "python-mpi4py")
    (version "3.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "mpi4py" version))
       (sha256
        (base32 "101lz7bnm9l17nrkbg6497kxscyh53aah7qd2b820ck2php8z18p"))))
    (build-system python-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'build 'mpi-setup
           ,%openmpi-setup)
         (add-before 'check 'pre-check
           (lambda _
             ;; Skip BaseTestSpawn class (causes error 'ompi_dpm_dyn_init()
             ;; failed --> Returned "Unreachable"' in chroot environment).
             (substitute* "test/test_spawn.py"
               (("unittest.skipMPI\\('openmpi\\(<3.0.0\\)'\\)")
                "unittest.skipMPI('openmpi')"))
             #t)))))
    (inputs
     (list openmpi))
    (home-page "https://bitbucket.org/mpi4py/mpi4py/")
    (synopsis "Python bindings for the Message Passing Interface standard")
    (description "MPI for Python (mpi4py) provides bindings of the Message
Passing Interface (MPI) standard for the Python programming language, allowing
any Python program to exploit multiple processors.

mpi4py is constructed on top of the MPI-1/MPI-2 specification and provides an
object oriented interface which closely follows MPI-2 C++ bindings.  It
supports point-to-point and collective communications of any picklable Python
object as well as optimized communications of Python objects (such as NumPy
arrays) that expose a buffer interface.")
    (license license:bsd-3)))

(define-public python-mpi4py-notest
  (package (inherit python-mpi4py)
    (name "python-mpi4py-notest")
    ; (inputs (modify-inputs (package-inputs python-mpi4py)
    ;   (replace "python" python3.8)))
    (arguments
          `(#:tests? #f))))

(define-public python-more-itertools-8.4
  (package (inherit python-more-itertools)
    (version "8.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "more-itertools" version))
         (sha256
           (base32
             "1r8pibxmwndv34wkhrgmdfiwd2bhhfkm8scgkmy5rpvv2v3hriv8"))))))

(define-public python-soundfile
  (package
    (name "python-soundfile")
    (version "0.10.3.post1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "SoundFile" version))
        (sha256
          (base32
            "0yqhrfz7xkvqrwdxdx2ydy4h467sk7z3gf984y1x2cq7cm1gy329"))))
    (build-system python-build-system)
    (arguments
          `(#:tests? #f))
    (propagated-inputs
      `(("python-cffi" ,python-cffi)
         ("libsndfile" ,libsndfile)
         ("python-numpy" ,python-numpy)))
    (home-page
      "https://github.com/bastibe/PySoundFile")
    (synopsis
      "An audio library based on libsndfile, CFFI and NumPy")
    (description
      "An audio library based on libsndfile, CFFI and NumPy")
    (license #f)))

(define-public python-pysndfile
  (package
    (name "python-pysndfile")
    (version "1.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pysndfile" version))
        (sha256
          (base32
            "00gr2q03l4mhz2d8idg2rslfrl8swjqcap5djhp891lxvs52zrqy"))))
    (build-system python-build-system)
    (arguments
          `(#:tests? #f))
    (propagated-inputs
      `(("python-numpy" ,python-numpy)
        ("libsndfile" ,libsndfile)))
    (native-inputs
      `(("python-cython" ,python-cython)))
    (home-page
      "https://forge-2.ircam.fr/roebel/pysndfile.git")
    (synopsis
      "pysndfile provides PySndfile, a Cython wrapper class for reading/writing soundfiles using libsndfile")
    (description
      "pysndfile provides PySndfile, a Cython wrapper class for reading/writing soundfiles using libsndfile")
    (license #f)))

(define-public python-fastcluster
  (package
    (name "python-fastcluster")
    (version "1.1.28")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "fastcluster" version))
        (sha256
          (base32
            "0ln20rhsj3mw15phrfm2rl0ss7b8db9sxjhg4d0g2386xill2a8h"))))
    (build-system python-build-system)
    (arguments
          `(#:tests? #f))
    (propagated-inputs
      `(("python-numpy" ,python-numpy)))
    (native-inputs `(("python-scipy" ,python-scipy)))
    (home-page "http://danifold.net")
    (synopsis
      "Fast hierarchical clustering routines for R and Python.")
    (description
      "Fast hierarchical clustering routines for R and Python.")
    (license #f)))

(define-public python-pycwt
  (package
    (name "python-pycwt")
    (version "0.3.0a22")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pycwt" version))
        (sha256
          (base32
            "05zb8m22dr8jva01wpkwmqwd0hp5dv4brjvbxwh513dg5kpni5pr"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f))
    (propagated-inputs
      `(("python-matplotlib" ,python-matplotlib)
        ("python-numpy" ,python-numpy)
        ("python-scipy" ,python-scipy)
        ("python-tqdm" ,python-tqdm)))
    (home-page "https://github.com/regeirk/pycwt")
    (synopsis
      "Continuous wavelet transform module for Python.")
    (description
      "Continuous wavelet transform module for Python.")
    (license license:bsd-3)))

(define-public python-ipdb
  (package
    (name "python-ipdb")
    (version "0.13.9")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "ipdb" version))
        (sha256
          (base32
            "1ibql99agjf2gj7y0svzd5m0h81hailf4p3sj3yl9i1i8ykdj6wm"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f))
    (propagated-inputs
      `(("python-setuptools" ,python-setuptools)))
    (home-page "https://github.com/gotcha/ipdb")
    (synopsis "IPython-enabled pdb")
    (description "IPython-enabled pdb")
    (license license:bsd-3)))

(define-public python-pygmt
  (package
    (name "python-pygmt")
    (version "0.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pygmt" version))
        (sha256
          (base32 "0aijdp8qbwnalqm9s3jyfmidibbwvwpgcgxk1hmnxw15ywn3fz92"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f
        #:phases
          (modify-phases %standard-phases
            ;; solve sanity-check crash on homeless-shelter not found, ugly but works
            (delete 'fix-and-disable-failing-tests)
            (delete 'sanity-check))))
    (propagated-inputs
      (list python-netcdf4
            python-numpy
            python-packaging
            python-pandas
            python-xarray
            gmt))
    (home-page "https://github.com/GenericMappingTools/pygmt")
    (synopsis "A Python interface for the Generic Mapping Tools")
    (description
      "This package provides a Python interface for the Generic Mapping Tools")
    (license license:bsd-3)))

(define-public python-pyvo
  (package
    (name "python-pyvo")
    (version "1.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pyvo" version))
        (sha256
          (base32
            "0azbdnpwpzfiyaf54531163gh2d8qqg313zlawfvfj7crdz54kz1"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f))
    (propagated-inputs
      `(("python-astropy" ,python-astropy)
        ("python-mimeparse" ,python-mimeparse)
        ("python-requests" ,python-requests)))
    (home-page "https://github.com/astropy/pyvo")
    (synopsis
      "Astropy affiliated package for accessing Virtual Observatory data and services")
    (description
      "Astropy affiliated package for accessing Virtual Observatory data and services")
    (license license:bsd-3)))

(define-public python-astropy-helpers
  (package
    (name "python-astropy-helpers")
    (version "4.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "astropy-helpers" version))
        (sha256
          (base32
            "04zlx56yg56138rm392n734gfbvvki6nv85ysqc84xq8s4a682gi"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f))
    (home-page
      "https://github.com/astropy/astropy-helpers")
    (synopsis
      "Utilities for building and installing packages in the Astropy ecosystem")
    (description
      "Utilities for building and installing packages in the Astropy ecosystem")
    (license #f)))

(define-public python-astroquery
  (package
    (name "python-astroquery")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/astropy/astroquery/archive/refs/tags/v" version ".tar.gz"))
        (sha256
          (base32
            "0yv5wcqfiicgwp1q83j694sqy3xb0b0xihcbpdivbm628sbl3jm4"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f))
    (propagated-inputs
      `(("python-astropy" ,python-astropy)
        ("python-astropy-helpers" ,python-astropy-helpers)
        ("python-beautifulsoup4" ,python-beautifulsoup4)
        ("python-html5lib" ,python-html5lib)
        ("python-keyring" ,python-keyring)
        ("python-numpy" ,python-numpy)
        ("python-pyvo" ,python-pyvo)
        ("python-requests" ,python-requests)
        ("python-six" ,python-six)))
    (native-inputs
      `(("python-flask" ,python-flask)
        ("python-jinja2" ,python-jinja2)
        ("python-matplotlib" ,python-matplotlib)))
    (home-page "http://astropy.org/astroquery")
    (synopsis
      "Functions and classes to access online astronomical data resources")
    (description
      "Functions and classes to access online astronomical data resources")
    (license license:bsd-3)))


(define-public python-vmd
  (package
    (name "python-vmd")
    (version "3.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/Eigenstate/vmd-python/archive/refs/tags/v" version ".tar.gz"))
        (sha256
          (base32
            "1gahymqjypnbcb3b5yppphdcqhvqdxghi8ab57dxjyhkyn7j565v"))))
    (build-system python-build-system)
    (arguments 
        '(#:tests? #f
          #:phases
            (modify-phases %standard-phases
              (add-before 'build 'fix_perl_interp
                (lambda* (#:key inputs #:allow-other-keys)
                  (substitute* "vmd/vmd_src/configure"
                    (("/usr/bin/env perl") (which "perl"))
                    (("SHELL                   = /bin/sh")(string-append "SHELL                   = " (which "sh")))))))))
    (propagated-inputs
      `(("python-numpy" ,python-numpy)
        ("netcdf" ,netcdf)
        ("sqlite" ,sqlite)
        ("tcl" ,tcl)
        ("perl" ,perl)
        ("expat" ,expat)))
    (home-page
      "https://vmd.robinbetz.com/index.html")
    (synopsis "VMD is an excellent visualization program, with powerful command-line scripting functionality.")
    (description "VMD is an excellent visualization program, with powerful command-line scripting functionality. Recently, VMD’s C functions can be invoked using a set of easy to use python modules. Vmd-python is installable VMD as a Python module.")
    (license license:expat)))
