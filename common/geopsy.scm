(define-module (common geopsy)
        #:use-module ((guix licenses) #:prefix license:)
        #:use-module (guix packages)
        #:use-module (guix download)
        #:use-module (guix utils)
        #:use-module (guix build utils)
        #:use-module (guix build-system gnu)
        #:use-module (gnu packages)
        #:use-module (gnu packages pkg-config)
        #:use-module (gnu packages qt)
        #:use-module (gnu packages c)
        #:use-module (gnu packages readline)
        #:use-module (gnu packages gcc)
        #:use-module (gnu packages xml)
        #:use-module (gnu packages compression)
        #:use-module (gnu packages boost)
        #:use-module (gnu packages algebra)
        #:use-module (gnu packages maths)
        #:use-module (gnu packages mpi)
        #:use-module (gnu packages sphinx)
        #:use-module (gnu packages commencement)
        #:use-module (gnu packages multiprecision))

(define-public geopsy
  (package
  (name "geopsy")
  (version "3.4.2")
  (source (origin
            (method url-fetch)
            (uri (string-append "https://www.geopsy.org/download/archives/geopsypack-src-3.4.2.tar.gz"))
            (sha256
             (base32
              "07v2dgbzc8yaz85cdmlx7p289hm8c8a20fki8sfwn0gy6bz180ay"))))
  (build-system gnu-build-system)
  (arguments
   `(#:configure-flags
      (list "-accept-license"
            "-builddir ./build"
            (string-append "-prefix "
                           (assoc-ref %outputs "out")))
     #:phases
      (modify-phases %standard-phases
        (add-after 'unpack 'post-unpack
          (lambda* (#:key outputs inputs #:allow-other-keys)
                  (mkdir-p "./build")
                  ))
        (replace 'configure
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (let ((out (assoc-ref outputs "out"))
                  (gfortran (assoc-ref inputs "libgfortran")))
              (apply invoke "./configure" (list "-accept-license" 
                                                "-builddir" "./build" 
                                                "-prefix" out 
                                                "-march" "native" "-L"
                                                (string-append gfortran "/lib"))))))
        (replace 'build
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
                 (apply invoke "make" (list "-j" "4" "-C" "./build")))))
        (replace 'install
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
                 (apply invoke "make" (list "install" "-C" "./build"))))))
      #:tests? #f))
  (inputs
    `(("libgfortran" ,gfortran "lib")
      ("gcc-toolchain" ,gcc-toolchain)
      ("zlib" ,zlib)
      ("fftw" ,fftw)
      ("qtbase" ,qtbase-5)
      ("qttools" ,qttools-5)
      ("qtsvg" ,qtsvg-5)
      ("qtdeclarative" ,qtdeclarative-5)
      ("lapack" ,lapack))
  )
  (synopsis "Geopsy team is developing, distributing and maintaining open source software for geophysical research and application")
  (description
   "Geopsy team is developing, distributing and maintaining open source software for geophysical research and applications. Born during SESAME European Project, it has provided tools for processing ambient vibrations with site characterization in mind since 2005. Progressively, more conventional techniques (such as MASW or refraction) are included to offer a high quality, comprehensive and free platform for the interpretation of geophysical experiments.")
  (home-page "https://www.geopsy.org")
  (license license:gpl3+)))