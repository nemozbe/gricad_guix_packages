(define-module (common lammps)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (guix utils)
      #:use-module (guix build utils)
      #:use-module (guix build-system cmake)
      #:use-module (guix build-system gnu)
      #:use-module (gnu packages)
      #:use-module (gnu packages base)
      #:use-module (gnu packages algebra)
      #:use-module (gnu packages image-processing)
      #:use-module (gnu packages image)
      #:use-module (gnu packages graphics)
      #:use-module (gnu packages maths)
      #:use-module (gnu packages mpi)
      #:use-module (gnu packages cmake)
      #:use-module (gnu packages python)
      #:use-module (gnu packages python-xyz)
      #:use-module (gnu packages compression)
      #:use-module (gnu packages commencement)
      #:use-module (gnu packages gcc)
      #:use-module (gnu packages vim)
      #:use-module (gnu packages pkg-config)
      #:use-module (gnu packages video)
      #:use-module (gnu packages llvm)
      #:use-module (gnu packages multiprecision))


(define-public n2p2-lib
 (package
    (name "n2p2-lib")
    (version "2.1.4")
    (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/CompPhysVienna/n2p2/archive/refs/tags/v" version ".tar.gz"))
            (sha256
             (base32
              "1vql7awa9yq795bxkgq4izlzx83m8rz9f1k675znmlafmw4jqrzi"))))
    (build-system gnu-build-system)
      (arguments
        `(#:phases
         (modify-phases %standard-phases
          (add-after 'unpack 'post-unpack
            (lambda* (#:key outputs inputs #:allow-other-keys)
              (chdir "./src")
              (substitute* "makefile.gnu"
                (("PROJECT_EIGEN=/usr/include/eigen3")
                 (string-append "PROJECT_EIGEN="
                                (assoc-ref inputs "eigen") "/include/eigen3")))
              (substitute* "makefile.gnu"
                (("-lblas")
                 (string-append "-L"
                                (assoc-ref inputs "openblas") "/lib -lopenblas")))))
           (delete 'configure)
           (delete 'check)
           (replace 'build
             (lambda _
               (invoke "make" "MODE=shared")))
           (replace 'install
             (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                    (bindir (string-append out "/bin"))
                    (libdir (string-append out "/lib"))
                    (incdir (string-append out "/include")))
                    (for-each (lambda (f) (install-file f libdir))
                             (find-files "../lib/" "."))
                    (for-each (lambda (f) (install-file f incdir))
                             (find-files "../include/" "."))))))))
    (inputs
    `(("openmpi" ,openmpi)
      ("gsl" ,gsl)
      ("openblas" ,openblas)
      ("eigen" ,eigen)))
  (synopsis "Classical molecular dynamics simulator")
  (description "LAMMPS is a classical molecular dynamics simulator designed to run efficiently on parallel computers.  LAMMPS has potentials for solid-state materials (metals, semiconductors), soft matter (biomolecules, polymers), and coarse-grained or mesoscopic systems.  It can be used to model atoms or, more generically, as a parallel particle simulator at the atomic, meso, or continuum scale.")
  (home-page "http://lammps.sandia.gov/")
  (license license:gpl2+)))


(define-public kim-api
  (package
    (name "kim-api")
    (version "2.3.0")
    (source (origin
	      (method url-fetch)
	      (uri (string-append "https://s3.openkim.org/kim-api/kim-api-" version ".txz"))
	      (sha256
		(base32
		  "14gr2hn8d5ii6wzcdaqcs5k36ybj2rfr2rzfya8mfqn0zfw3nrwk"))))
    (build-system cmake-build-system)
    (arguments
      `(#:configure-flags
        (list
          "-DCMAKE_BUILD_TYPE=Release"
          (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out")))
        #:phases
        (modify-phases %standard-phases
          (delete 'validate-runpath)                   
          (add-after 'unpack 'post-unpack
            (lambda* (#:key outputs inputs version #:allow-other-keys)
	      (invoke "tar" "xf" (string-append "kim-api-" ,version ".tar"))
              (mkdir-p "./build")
              (chdir "./build")
              (setenv "FC" (which "gfortran")) 
            )
          )
          (replace 'configure
            (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
                     (let ((out (assoc-ref outputs "out")))
                       (apply invoke "cmake" (string-append "../kim-api-" ,version ) configure-flags)
                     )
            )
          )
        )
        #:tests? #f
       )
    )
    ;; TODO how to install make (and others?) as a propagated-input if required to install user models?
    (propagated-inputs
      `(("tar" ,tar)
        ("gfortran" ,gfortran-toolchain)
        ("cmake" ,cmake)
        ;;("make" ,make)
        ("xxd" ,xxd)
        ("pkg-config" ,pkg-config)
        ("gcc-toolchain" ,gcc-toolchain-11)))
    (synopsis "Open Knowledgebase of Interatomic Models")
    (description "OpenKIM is a curated repository of interatomic potentials and analytics for making classical molecular simulations of materials reliable, reproducible, and accessible.")
    (home-page "https://openkim.org/")
    (license license:lgpl2.1+)))


(define-public lammps
  (package
  (name "lammps")
  (version "patch_2Jul2021")
  (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/lammps/lammps/archive/refs/tags/" version ".tar.gz"))
            (sha256
             (base32
              "0mzagwvrk97pyqjlpqpb487887xffc5na5l70k00k26yvfi8rpag"))))
  (build-system cmake-build-system)
  (arguments
   `(#:configure-flags
      (list "-DPKG_COMPRESS=yes"
      "-DPKG_USER-MISC=yes"
      "-DPKG_KSPACE=yes"
      "-DPKG_MC=yes"
      "-DPKG_KIM=yes"
      "-DPKG_MOLECULE=yes"
      "-DPKG_RIGID=yes"
      "-DPKG_MISC=yes"
      "-DPKG_MEAM=yes"
      "-DPKG_MANYBODY=yes"
      "-DPKG_REAXFF=yes"
      "-DPKG_ML-RANN=yes"
      "-DPKG_PHONON=yes"
      "-DPKG_ML-HDNNP=yes"
      "-DDOWNLOAD_N2P2=no"
      "-DDOWNLOAD_KIM=no"
      "-DPKG_DPD-BASIC=yes"
      "-DPKG_DIFFRACTION=yes"
      "-DPKG_EFF=yes"
      (string-append "-DN2P2_DIR=" (assoc-ref %build-inputs "n2p2-lib"))
      (string-append "-DKIM_DIR=" (assoc-ref %build-inputs "kim-api"))
      (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out")))
      #:phases
      (modify-phases %standard-phases
        (add-after 'unpack 'post-unpack
          (lambda* (#:key outputs inputs #:allow-other-keys)
            (mkdir-p "./build")
            (chdir "./build")))
        (replace 'configure
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
              (apply invoke "cmake" "../cmake" configure-flags)))))
      #:tests? #f))
  (inputs
    `(("python" ,python-wrapper)
      ("gfortran" ,gfortran)
      ("gcc" ,gcc)
      ("pkg-config" ,pkg-config)
      ("zlib" ,zlib)
      ("libomp" ,libomp)
      ("zstd" ,zstd)
      ("fftw" ,fftw)
      ("n2p2-lib" ,n2p2-lib)
      ("kim-api" ,kim-api)
      ("openmpi" ,openmpi)
      ("ffmpeg" ,ffmpeg)
      ("libpng" ,libpng)
      ("libjpeg" ,libjpeg-turbo)
      ("hdf5" ,hdf5)
      ("gzip" ,gzip)))
  (native-inputs
    `(("bc" ,bc)))
  (synopsis "Classical molecular dynamics simulator")
  (description "LAMMPS is a classical molecular dynamics simulator designed to run efficiently on parallel computers.  LAMMPS has potentials for solid-state materials (metals, semiconductors), soft matter (biomolecules, polymers), and coarse-grained or mesoscopic systems.  It can be used to model atoms or, more generically, as a parallel particle simulator at the atomic, meso, or continuum scale.")
  (home-page "http://lammps.sandia.gov/")
  (license license:gpl2+)))