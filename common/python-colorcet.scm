(define-module (common python-colorcet)
   #:use-module  ((guix licenses) #:prefix license:)
   #:use-module  (gnu packages)
   #:use-module  (guix packages)
   #:use-module  (guix utils)
   #:use-module  (guix download)
   #:use-module  (guix build-system python)
   #:use-module  (gnu packages python)
   #:use-module  (gnu packages python-xyz)
   #:use-module  (gnu packages python-crypto)
   #:use-module  (gnu packages python-check)
   #:use-module  (gnu packages python-science)
   #:use-module  (gnu packages python-web)
   #:use-module  (gnu packages check)
   #:use-module  (gnu packages graph)
   #:use-module  (gnu packages databases)
   #:use-module  (gnu packages xml)
   #:use-module  (gnu packages compression)
   #:use-module  (gnu packages python-build))


(define-public python-pyct
  (package
    (name "python-pyct")
    (version "0.4.8")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pyct" version))
        (sha256
          (base32 "1xzndlib2f5pdrzxg381bym1b5406ff4psis15f56rqmb9dm5mr3"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs (list python-param))
    (native-inputs (list python-flake8))
    (home-page "https://github.com/pyviz-dev/pyct")
    (synopsis
      "Python package common tasks for users (e.g. copy examples, fetch data, ...)")
    (description
      "Python package common tasks for users (e.g.  copy examples, fetch data, ...)")
    (license #f)))

(define-public python-param
  (package
    (name "python-param")
    (version "1.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "param" version))
        (sha256
          (base32 "13sgp2sgcjzndgjzkylzvpw39w3ikw0bx7qmh70z8wiign1z2lya"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (native-inputs (list python-flake8))
    (home-page "http://param.holoviz.org/")
    (synopsis
      "Make your Python code clearer and more reliable by declaring Parameters.")
    (description
      "Make your Python code clearer and more reliable by declaring Parameters.")
    (license license:bsd-3)))

(define-public python-colorcet
  (package
    (name "python-colorcet")
    (version "3.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "colorcet" version))
        (sha256
          (base32 "1nzkzhnpvqk1jz2h7hy8yhi7hgxc49n9kwi96xh1ma3sd8s25i91"))))
    (build-system python-build-system)
    (arguments
      '(#:tests? #f))
    (propagated-inputs (list python-param python-pyct))
    (native-inputs
      (list python-flake8
            python-param
            python-pyct
            python-twine))
    (home-page "https://colorcet.holoviz.org")
    (synopsis "Collection of perceptually uniform colormaps")
    (description "Collection of perceptually uniform colormaps")
    (license #f)))


