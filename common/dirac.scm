(define-module (common dirac)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages python)
  #:use-module (gnu packages version-control)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu))

;; On définit les paquets accessibles avec define-public

(define-public dirac
    (package
        (name "dirac")
        (version "19.0")
        (source (origin
            (method url-fetch)
            (uri (string-append "https://zenodo.org/record/3572669/files/DIRAC-" version "-Source.tar.gz"))
            (sha256
                (base32
                    "0cic817i2kdwqhsxdxrsacppd942fpyz6k7vrn8gzg0zvl5n3qzh"))))
        (build-system gnu-build-system)
        (propagated-inputs 
          `(("python-wrapper" ,python-wrapper)
            ("perl" ,perl)
            ("ksh" ,oksh)
            ("csh" ,tcsh)
            ("gcc" ,gcc-toolchain)
            ("gfortran" ,gfortran-toolchain)
            ("cmake" ,cmake)
            ("git" ,git)
            ("openblas" ,openblas)
            ("openmpi" ,openmpi)))
        (arguments
          `(#:tests? #f                                ; no tests to run
            #:phases (modify-phases %standard-phases
                       (replace 'configure
                         (lambda _
                           (and (invoke "./setup" "--blas=auto" "--lapack=auto"  "--mpi" (string-append "--prefix="
                                              (assoc-ref %outputs "out")))
                                (chdir "build")))))))
        (synopsis "DIRAC: Program for Atomic and Molecular Direct Iterative Relativistic All-electron Calculations.")
        (description
            "The DIRAC program computes molecular properties using relativistic quantum chemical methods. It is named after P.A.M. Dirac, the father of relativistic electronic structure theory.")
        (home-page "http://www.diracprogram.org/doku.php")
        (license license:gpl3+)))