(define-module (common cpan)
  #:use-module (guix licenses) 
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix build-system perl)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages assembly)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages statistics))

(define-public perl-local-lib 
 (package
  (name "perl-local-lib")
  (version "2.000029")
  (source (origin
            (method url-fetch)
            (uri (string-append
                  "mirror://cpan/authors/id/H/HA/HAARG/local-lib-" version
                  ".tar.gz"))
            (sha256
             (base32
              "1mzx7srjpc090z48f7i5wlcxb1w19cg71ia7bff913jcq487my4d"))))
  (build-system perl-build-system)
  (propagated-inputs (list perl-module-build))
  (home-page "https://metacpan.org/release/local-lib")
  (synopsis "create and use a local lib/ for perl modules with PERL5LIB")
  (description "create and use a local lib/ for perl modules with PERL5LIB")
  (license perl-license)))