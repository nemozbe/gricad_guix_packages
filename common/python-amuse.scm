(define-module (common python-amuse)
             #:use-module ((guix licenses) #:prefix license:)
             #:use-module (gnu packages)
             #:use-module (guix packages)
             #:use-module (guix utils)
             #:use-module (guix gexp)
             #:use-module (guix download)
             #:use-module (guix git-download)
             #:use-module (guix build-system python)
             #:use-module (guix build-system gnu)
             #:use-module (gnu packages base)
             #:use-module (gnu packages gcc)
             #:use-module (gnu packages perl)
             #:use-module (gnu packages node)
             #:use-module (gnu packages python)
             #:use-module (gnu packages algebra)
             #:use-module (gnu packages python-web)
             #:use-module (gnu packages commencement)
             #:use-module (gnu packages python-science)
             #:use-module (gnu packages xml)
             #:use-module (gnu packages cmake)
             #:use-module (gnu packages multiprecision)
             #:use-module (gnu packages maths)
             #:use-module (gnu packages elf)
             #:use-module (gnu packages glib)
             #:use-module (gnu packages databases)
             #:use-module (gnu packages check)
             #:use-module (gnu packages protobuf)
             #:use-module (gnu packages mpi)
             #:use-module (gnu packages ssh)
             #:use-module (gnu packages gtk)
             #:use-module (gnu packages machine-learning)
             #:use-module (gnu packages bootstrap)
             #:use-module (gnu packages pkg-config)
             #:use-module (gnu packages python-check)
             #:use-module (gnu packages python-crypto)
             #:use-module (gnu packages python-xyz)
             #:use-module (gnu packages time)
             #:use-module (gnu packages libidn)
             #:use-module (gnu packages libffi)
             #:use-module (gnu packages pulseaudio)
             #:use-module (gnu packages python-build)
             #:use-module (gnu packages compression)
             #:use-module (gnu packages bioinformatics)
             #:use-module (gnu packages geo))

(define-public python-amuse-twobody
  (package
    (name "python-amuse-twobody")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-twobody" version))
              (sha256
               (base32
                "1jcmzd5cyv9xzz8g8fhh5lna090y44v6nzcwpjnpy4bxs1n3a1ki"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - twobody")
    (description
     "The Astrophysical Multipurpose Software Environment - twobody")
    (license #f)))
(define-public python-amuse-sse
  (package
    (name "python-amuse-sse")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-sse" version))
              (sha256
               (base32
                "06hnmbs5nbiv9cnh5l5w48637rs9gs43nph6mnf1g73rh1g7nbla"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - SSE")
    (description "The Astrophysical Multipurpose Software Environment - SSE")
    (license #f)))
(define-public python-amuse-sphray
  (package
    (name "python-amuse-sphray")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-sphray" version))
              (sha256
               (base32
                "1y1bxkh9bh2rdvydsip052f822r40qra4cgvvpkkxsw9zf016nwz"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - SPHRay")
    (description
     "The Astrophysical Multipurpose Software Environment - SPHRay")
    (license #f)))
(define-public python-amuse-smalln
  (package
    (name "python-amuse-smalln")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-smalln" version))
              (sha256
               (base32
                "18s31qp6ls5plsfdms54gg858469q43z01wb5xqc6ls74dcnaa5q"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - smalln")
    (description
     "The Astrophysical Multipurpose Software Environment - smalln")
    (license #f)))
(define-public python-amuse-simplex
  (package
    (name "python-amuse-simplex")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-simplex" version))
              (sha256
               (base32
                "1icswn0rjx8pxbxp7qffd1hziz9b6vsh1q4kw4dqxa0nyszbyylk"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework python-mpi4py))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  hdf5
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Simplex")
    (description
     "The Astrophysical Multipurpose Software Environment - Simplex")
    (license #f)))
(define-public python-amuse-secularmultiple
  (package
    (name "python-amuse-secularmultiple")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-secularmultiple" version))
              (sha256
               (base32
                "1vvhldgxspd50qqww2kfzsq6qx6cd8gnz31svnvg7kgg5pbc9gh9"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis
     "The Astrophysical Multipurpose Software Environment - SecularMultiple")
    (description
     "The Astrophysical Multipurpose Software Environment - SecularMultiple")
    (license #f)))

(define-public python-amuse-seba
  (package
    (name "python-amuse-seba")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri "https://github.com/amusecode/SeBa/archive/ae0f156f7844893d134808f0c9635b9cc39f61e4.tar.gz")
              (sha256
               (base32
                "1ad14q85lq16n68abvqq9dj4av71wrn7xwhb3i98wnypqpm26q36"))))
    (build-system gnu-build-system)
    (propagated-inputs (list python-amuse-framework))
    (arguments
      '(#:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (delete 'check)
           (replace 'build
             (lambda* (#:key outputs #:allow-other-keys)
               (chdir "dstar/")
               (chdir "..")
               (substitute* "Makefile" (("/bin/rm") "rm"))
               (substitute* "Makefile.inc.conf" (("-I__BASE__/include -I__BASE__/include/star")
                                                  "-I$(PWD)/include -I$(PWD)/include/star -I$(PWD)/../include -I$(PWD)/../../include -I$(PWD)/../include/star"))
               (substitute* "dstar/Makefile" (("/bin/rm") "rm"))
               (substitute* "node/Makefile" (("/bin/rm") "rm"))
               (substitute* "node/dyn/Makefile" (("/bin/rm") "rm"))
               (substitute* "rdc/Makefile" (("/bin/rm") "rm"))
               (substitute* "sstar/Makefile" (("/bin/rm") "rm"))
               (invoke "make" "clean")
               (invoke "make")
               (chdir "dstar/")
               (invoke "make" "clean")
               (invoke "make")))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
                (install-file "./SeBa" (string-append (assoc-ref outputs "out") "/bin/")))))))

    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - SeBa")
    (description "The Astrophysical Multipurpose Software Environment - SeBa")
    (license #f)))

(define-public python-amuse-phigrape
  (package
    (name "python-amuse-phigrape")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-phigrape" version))
              (sha256
               (base32
                "10xrcl1ldwxcf4wh1g9s0qfdj18qcw5vfcizvjpwzf45ba5cm55a"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - phiGRAPE")
    (description
     "The Astrophysical Multipurpose Software Environment - phiGRAPE")
    (license #f)))
(define-public python-amuse-ph4
  (package
    (name "python-amuse-ph4")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-ph4" version))
              (sha256
               (base32
                "1vy2xhpzh6zqs7q715s44y0rfm19nz9hkzk22c97vfqxq6jc70kc"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - ph4")
    (description "The Astrophysical Multipurpose Software Environment - ph4")
    (license #f)))
(define-public python-amuse-mmams
  (package
    (name "python-amuse-mmams")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-mmams" version))
              (sha256
               (base32
                "084hxsqwsb2xmjf4kx9wkgcany8aglzqjq5dsp9hdi4z4l289z5s"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  cmake
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - MMAMS")
    (description "The Astrophysical Multipurpose Software Environment - MMAMS")
    (license #f)))
(define-public python-amuse-mercury
  (package
    (name "python-amuse-mercury")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-mercury" version))
              (sha256
               (base32
                "0raqmwqp164v3dddxhzwfspvja0i0hmb7gdp7yksqv1qfc0f4cvr"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Mercury")
    (description
     "The Astrophysical Multipurpose Software Environment - Mercury")
    (license #f)))
(define-public python-amuse-mameclot
  (package
    (name "python-amuse-mameclot")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-mameclot" version))
              (sha256
               (base32
                "0l197khsp5fv3p7cj4smmvslx14prqpywpnyd51057vhkhjlfgjg"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Mameclot")
    (description
     "The Astrophysical Multipurpose Software Environment - Mameclot")
    (license #f)))
(define-public python-amuse-kepler-orbiters
  (package
    (name "python-amuse-kepler-orbiters")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-kepler-orbiters" version))
              (sha256
               (base32
                "1wzdxijbmzgm03m1ghbm0rj57qnmhqyapd0fmffsg9vvfn6vh4dv"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis
     "The Astrophysical Multipurpose Software Environment - Kepler-orbiters")
    (description
     "The Astrophysical Multipurpose Software Environment - Kepler-orbiters")
    (license #f)))
(define-public python-amuse-kepler
  (package
    (name "python-amuse-kepler")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-kepler" version))
              (sha256
               (base32
                "14ch4dhlyzgd4ikccrgw8pv4k0ajygbkznlmrc1240gxzvq3y4vj"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Kepler")
    (description
     "The Astrophysical Multipurpose Software Environment - Kepler")
    (license #f)))
(define-public python-amuse-huayno
  (package
    (name "python-amuse-huayno")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-huayno" version))
              (sha256
               (base32
                "073h30xd27jp8j6lk8gc8y8qxq6agkkqmhhnnqv37pwsvs2jqvqd"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Huayno")
    (description
     "The Astrophysical Multipurpose Software Environment - Huayno")
    (license #f)))
(define-public python-amuse-hop
  (package
    (name "python-amuse-hop")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-hop" version))
              (sha256
               (base32
                "0yc3lhdaxa5svl28g9d05hrg21r8qm6alr0cawz4arq0wns0g1dz"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Hop")
    (description "The Astrophysical Multipurpose Software Environment - Hop")
    (license #f)))
(define-public python-amuse-hermite
  (package
    (name "python-amuse-hermite")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-hermite" version))
              (sha256
               (base32
                "0klksbxngnnm82pcnj7gh6dwmpcpnx8v2mkawm4rg35r3in201s2"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Hermite")
    (description
     "The Astrophysical Multipurpose Software Environment - Hermite")
    (license #f)))
(define-public python-amuse-halogen
  (package
    (name "python-amuse-halogen")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-halogen" version))
              (sha256
               (base32
                "0n20mgx105bn4i0h8076xmvxdbs3ff4h1q0029i9pnmdxrr8hpik"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Halogen")
    (description
     "The Astrophysical Multipurpose Software Environment - Halogen")
    (license #f)))
(define-public python-amuse-galaxia
  (package
    (name "python-amuse-galaxia")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-galaxia" version))
              (sha256
               (base32
                "062mrvxnfdsvfdgij102qiayg30rrlc9s1jv1yh5fw5kp94gccrf"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openblas
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Galaxia")
    (description
     "The Astrophysical Multipurpose Software Environment - Galaxia")
    (license #f)))
(define-public python-amuse-galactics
  (package
    (name "python-amuse-galactics")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-galactics" version))
              (sha256
               (base32
                "0rfmbmf7dswixhvs2zwk0zw3d82vazql6p4fpf6mgvphlvn5wb74"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis
     "The Astrophysical Multipurpose Software Environment - Galactics")
    (description
     "The Astrophysical Multipurpose Software Environment - Galactics")
    (license #f)))
(define-public python-amuse-gadget2
  (package
    (name "python-amuse-gadget2")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-gadget2" version))
              (sha256
               (base32
                "0a835s5w2n7l2wnpzxaxzlskrp1z67hk49pshxz7wn87538g19s1"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Gadget2")
    (description
     "The Astrophysical Multipurpose Software Environment - Gadget2")
    (license #f)))
(define-public python-amuse-fractalcluster
  (package
    (name "python-amuse-fractalcluster")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-fractalcluster" version))
              (sha256
               (base32
                "14yd563v448nq8ci9p11fjdjsrq2fq3hq9rzc09656npd383g4d5"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis
     "The Astrophysical Multipurpose Software Environment - fractalcluster")
    (description
     "The Astrophysical Multipurpose Software Environment - fractalcluster")
    (license #f)))
(define-public python-amuse-fi
  (package
    (name "python-amuse-fi")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-fi" version))
              (sha256
               (base32
                "0qhpx42kj1b48if96vff1s70jqvx1s9y5v18w8a29vlr1risdvca"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  fftw-fortran
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - FI")
    (description "The Astrophysical Multipurpose Software Environment - FI")
    (license #f)))
(define-public python-amuse-fastkick
  (package
    (name "python-amuse-fastkick")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-fastkick" version))
              (sha256
               (base32
                "020ipp8kygawakq09a9vshcpq1rhmanfh83r85zpvj7ikb60x6j2"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - fastkick")
    (description
     "The Astrophysical Multipurpose Software Environment - fastkick")
    (license #f)))
(define-public python-amuse-evtwin
  (package
    (name "python-amuse-evtwin")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-evtwin" version))
              (sha256
               (base32
                "1zcd5bx214y151kxqpkp7v0fp5a8la5zw0vpv4whb1j4chwina5p"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  cmake
		  perl
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - EVTwin")
    (description
     "The Astrophysical Multipurpose Software Environment - EVTwin")
    (license #f)))
(define-public python-amuse-capreole
  (package
    (name "python-amuse-capreole")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-capreole" version))
              (sha256
               (base32
                "1d79zqi70v3y1nhydlbxsfp0mslgqd8f4qi2vs6sksra5yrqhm2v"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Capreole")
    (description
     "The Astrophysical Multipurpose Software Environment - Capreole")
    (license #f)))

(define-public python-amuse-bse
  (package
    (name "python-amuse-bse")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-bse" version))
              (sha256
               (base32
                "1vaq3hwz3xbqk25dg4xl6il7sv4lwihw89749r9fzy3ybfycz9r4"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  gfortran-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - BSE")
    (description "The Astrophysical Multipurpose Software Environment - BSE")
    (license #f)))

(define-public python-amuse-bhtree
  (package
    (name "python-amuse-bhtree")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-bhtree" version))
              (sha256
               (base32
                "1z3cxh0g7p2h816vdkn28v62spcfs4xfwwdag9g6kgbhhcd7ryxg"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  openmpi))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - BHTree")
    (description
     "The Astrophysical Multipurpose Software Environment - BHTree")
    (license #f)))

(define-public python-amuse-framework
  (package
    (name "python-amuse-framework")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-framework" version))
              (sha256
               (base32
                "0wshdaq5ncr20pmh5hf0clv66075xzj9qcg6iacg2nnr4hlpk7ys"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (inputs (list gcc-toolchain
			 gfortran-toolchain
			 cmake
			 gmp
			 hdf5
			 openmpi
			 python
			 gsl
			 mpfr
			 fftw
			 netcdf
			 lapack
			 openblas))
    (propagated-inputs (list python-docutils
                             python-h5py
                             python-numpy
                             python-pip
                             python-pytest
                             python-setuptools
                             python-setuptools-scm
                             python-wheel))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment")
    (description "The Astrophysical Multipurpose Software Environment")
    (license #f)))

(define-public python-amuse-athena
  (package
    (name "python-amuse-athena")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-athena" version))
              (sha256
               (base32
                "1hpk71m2y82xbiknk3p67fccr957qfcrhl95ajhb57yi60yh2dgb"))))
    (build-system python-build-system)
    (arguments
       `(#:phases
          (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
            (delete 'fix-and-disable-failing-tests)
            (delete 'sanity-check)
            (delete 'check)
	   (add-before 'build 'pre-configure
              (lambda _
                (setenv "CONFIG_SHELL" (which "sh"))
		(setenv "SHELL" (which "sh"))))
	 (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (invoke "python" "setup.py" "install"
                     (string-append "--prefix=" (assoc-ref outputs "out"))
                     "--root=/")))))) 
    (inputs (list gcc-toolchain
		  openmpi))
    (propagated-inputs (list python-amuse-framework))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - Athena")
    (description
     "The Astrophysical Multipurpose Software Environment - Athena")
    (license #f)))

(define-public phantom-src
  (package
    (name "phantom-src")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri "https://github.com/rieder/phantom/archive/ca6907f5f9a95f866b1d7e520cc356ab8cec8dd0.tar.gz")
              (sha256
               (base32
                "1alwl4kipp2whclw7428pngv738jsvhlvn29k6q18ddd8j6q24cx"))))
    (build-system gnu-build-system)
    (native-inputs (list python))
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'configure)
           (delete 'build)
           (delete 'sanity-check)
           (delete 'check)
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out")))
                             (copy-recursively "." out)))))))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - phantom sources")
    (description "The Astrophysical Multipurpose Software Environment - phantom sources")
    (license #f)))

(define-public python-amuse-phantom
  (package
    (name "python-amuse-phantom")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-phantom" version))
              (sha256
               (base32
                "029nqwrybfjhlkxnf7k054s5l77m4q1y9kyrakn53jj3fhylq3wc"))))
              ;(patches (search-patches "phantom.patch"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
           (add-after 'unpack 'patch-make
                      (lambda* (#:key inputs outputs #:allow-other-keys)
                               (let* ((phantom (assoc-ref inputs "phantom-src")))
                              (copy-recursively phantom "./phantom-src")
                              (chdir "./phantom-src")
                              (for-each make-file-writable (find-files "."))
                              (chdir "../")
                              (substitute* "src/amuse/community/phantom/Makefile"
                                            (("\\$\\(DOWNLOAD_FROM_WEB\\)") 
                                              "cp -r ../../../../phantom-src src/phantom")))))
           (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
               (setenv "SHELL" (which "sh"))))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/"))))))
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
                  openmpi
                  phantom-src
                  gfortran-toolchain
                  ))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - phantom")
    (description "The Astrophysical Multipurpose Software Environment - phantom")
    (license #f)))

(define-public python-amuse
  (package
    (name "python-amuse")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse" version))
              (sha256
               (base32
                "0mgi7zfsms8kmw94jja613hv2xpdrdxc8p0dppz8fmbpkdhywpyi"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-athena
                             python-amuse-bhtree
                             python-amuse-bse
                             python-amuse-phantom
                             python-amuse-capreole
                             python-amuse-evtwin
                             python-amuse-fastkick
                             python-amuse-fi
                             python-amuse-fractalcluster
                             python-amuse-framework
                             python-amuse-gadget2
                             python-amuse-galactics
                             python-amuse-galaxia
                             python-amuse-halogen
                             python-amuse-hermite
                             python-amuse-hop
                             python-amuse-huayno
                             python-amuse-kepler
                             python-amuse-kepler-orbiters
                             python-amuse-mameclot
                             python-amuse-mercury
                             python-amuse-mmams
                             python-amuse-petar
                             python-amuse-ph4
                             python-amuse-phigrape
                             python-amuse-seba
                             python-amuse-secularmultiple
                             python-amuse-simplex
                             python-amuse-smalln
                             python-amuse-sphray
                             python-amuse-sse
                             python-amuse-twobody
                             python-matplotlib
                             python-numpy))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment")
    (description "The Astrophysical Multipurpose Software Environment")
    (license #f)))

(define-public fftw-fortran
  (package
    (name "fftw-fortran")
    (version "3.3.10")
    (source (origin
             (method url-fetch)
             (uri (string-append "ftp://ftp.fftw.org/pub/fftw/fftw-"
                                 version".tar.gz"))
             (sha256
              (base32
               "0rv4w90b65b2kvjpj8g9bdkl4xqc42q20f5bzpxdrkajk1a35jan"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags
       '("--enable-shared" "--enable-openmp" "--enable-threads"
         ,@(let ((system (or (%current-target-system) (%current-system))))
             ;; Enable SIMD extensions for codelets.  See details at:
             ;; <http://fftw.org/fftw3_doc/Installation-on-Unix.html>.
             (cond
              ((string-prefix? "x86_64" system)
               '("--enable-sse2" "--enable-avx" "--enable-avx2"
                 "--enable-avx512" "--enable-avx-128-fma"))
              ((string-prefix? "i686" system)
               '("--enable-sse2"))
              ((string-prefix? "aarch64" system)
               ;; Note that fftw supports NEON on 32-bit ARM only when
               ;; compiled for single-precision.
               '("--enable-neon"))
              (else
               '())))
         ;; By default '-mtune=native' is used.  However, that may cause the
         ;; use of ISA extensions (e.g. AVX) that are not necessarily
         ;; available on the user's machine when that package is built on a
         ;; different machine.
         "ax_cv_c_flags__mtune_native=no")))
    (native-inputs (list perl))
    (inputs (list gfortran-toolchain))
    (home-page "http://fftw.org")
    (synopsis "Computing the discrete Fourier transform")
    (description
     "FFTW is a C subroutine library for computing the discrete Fourier
transform (DFT) in one or more dimensions, of arbitrary input size, and of
both real and complex data (as well as of even/odd data---i.e. the discrete
cosine/ sine transforms or DCT/DST).")
    (license license:gpl2+)))

(define-public fftw-fortran-openmpi
  (package/inherit fftw-fortran
    (name "fftw-fortran-openmpi")
    (inputs
     `(("openmpi" ,openmpi)
       ,@(package-inputs fftw-fortran)))
    (arguments
     (substitute-keyword-arguments (package-arguments fftw-fortran)
       ((#:configure-flags cf)
        `(cons "--enable-mpi" ,cf))
       ((#:phases phases '%standard-phases)
        `(modify-phases ,phases
           (add-before 'check 'mpi-setup
             ,%openmpi-setup)))))
    (description
     (string-append (package-description fftw-fortran)
                    "  With Fortran and OpenMPI parallelism support."))))


(define-public openblas-omp
  (package/inherit openblas
    (name "openblas-omp")
    (supported-systems '("x86_64-linux" "aarch64-linux" "mips64el-linux"
                         "powerpc64le-linux"))
    (arguments
     (substitute-keyword-arguments (package-arguments openblas)
       ((#:make-flags flags #~'())
        #~(append (list "USE_OPENMP=1")
                 #$flags))))
    (synopsis "Optimized BLAS library with OpenMP Support")
    (license license:bsd-3)))

(define-public petar-src
  (package
    (name "petar-src")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri "https://github.com/lwang-astro/PeTar/archive/22437b74c6541fc9451607fb2933558bd6924a41.tar.gz")
              (sha256
               (base32
                "1anq2x0kc59rbv0a1n36pjzvaa0g73i0i5fy2jw7vav3f8wscgn9"))))
    (build-system gnu-build-system)
    (native-inputs (list python))
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
	   (delete 'configure)
	   (delete 'build)
           (delete 'sanity-check)
           (delete 'check)
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
		      (let* ((out (assoc-ref outputs "out")))
			     (copy-recursively "." out)))))))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - PeTar")
    (description "The Astrophysical Multipurpose Software Environment - PeTar")
    (license #f)))


(define-public fdps-src
  (package
    (name "fdps-src")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri "https://github.com/FDPS/FDPS/archive/6fedb4b8bd7a504598e83a4189a7a83c533a0848.tar.gz")
              (sha256
               (base32
                "0f58kzkw6i6v34x1s7lakcqi0hxqns9vrmy7mc7h7iz3c719gzrf"))))
    (build-system gnu-build-system)
    (native-inputs (list python))
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
	   (delete 'configure)
	   (delete 'build)
           (delete 'sanity-check)
           (delete 'check)
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
		      (let* ((out (assoc-ref outputs "out")))
			     (copy-recursively "." out)))))))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - PeTar")
    (description "The Astrophysical Multipurpose Software Environment - PeTar")
    (license #f)))

(define-public sdar-src
  (package
    (name "sdar-src")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri "https://github.com/lwang-astro/SDAR/archive/cebf0a9cbd17111c6917e1120452bb9f661e33b0.tar.gz")
              (sha256
               (base32
                "0m38cxl9vcm09pbm0704cv6vwmnwhha824rcp0lvabzjin95597b"))))
    (build-system gnu-build-system)
    (native-inputs (list python))
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
	   (delete 'configure)
	   (delete 'build)
           (delete 'sanity-check)
           (delete 'check)
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
		      (let* ((out (assoc-ref outputs "out")))
			     (copy-recursively "." out)))))))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - PeTar")
    (description "The Astrophysical Multipurpose Software Environment - PeTar")
    (license #f)))

(define-public python-amuse-petar
  (package
    (name "python-amuse-petar")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-petar" version))
              (sha256
               (base32
                "165wlw8276011skq5dsy8lyw7li5c1bpaw3s0lark88lqw11d28m"))
	      ;(patches (search-patches "petar.patch"))
        ))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-after 'unpack 'patch-make
		      (lambda* (#:key inputs outputs #:allow-other-keys)
            		       (let* ((petar (assoc-ref inputs "petar-src"))
				                      (fdps (assoc-ref inputs "fdps-src"))
				                      (sdar (assoc-ref inputs "sdar-src")))
                              (copy-recursively petar "./petar-src")
                              (chdir "./petar-src")
                              (for-each make-file-writable (find-files "."))
                              (chdir "../")
                              (copy-recursively fdps "./fdps-src")
                              (chdir "./fdps-src")
                              (for-each make-file-writable (find-files "."))
                              (chdir "../")
                              (copy-recursively sdar "./sdar-src")
                              (chdir "./sdar-src")
                              (for-each make-file-writable (find-files "."))
                              (chdir "../")
				                      (substitute* "src/amuse/community/petar/Makefile"
                              (("\\$\\(DOWNLOAD_FROM_WEB\\)")
                                "cp -r ../../../../petar-src src/PeTar
\tcp -r ../../../../fdps-src src/FDPS
\tcp -r ../../../../sdar-src src/SDAR")))))
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  openmpi
		  sdar-src
		  fdps-src
		  petar-src))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - PeTar")
    (description "The Astrophysical Multipurpose Software Environment - PeTar")
    (license #f)))
