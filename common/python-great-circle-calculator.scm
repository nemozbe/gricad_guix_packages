(define-module (common python-great-circle-calculator)
  #:use-module  ((guix licenses) #:prefix license:)
  #:use-module  (gnu packages)
  #:use-module  (guix packages)
  #:use-module  (guix utils)
  #:use-module  (guix download)
  #:use-module  (guix build-system python)
  #:use-module  (gnu packages python)
  #:use-module  (gnu packages python-build))

(define-public python-great-circle-calculator
  (package
    (name "python-great-circle-calculator")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "great_circle_calculator" version))
        (sha256
          (base32 "1ksrsnr0z9hvhhmnyqlg0rmg482wc5gigz3hgvd4c1cy574rg2aq"))))
    (build-system python-build-system)
    (home-page "https://github.com/seangrogan/great_circle_calculator")
    (synopsis
      "A collection of functions to calculate attributes of the great circle")
    (description
      "This package provides a collection of functions to calculate attributes of the
great circle")
    (license #f)))
