(define-module (common r-frechet)
  #:use-module (guix licenses) 
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix build-system r)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages assembly)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages statistics))

(define-public r-osqp
  (package
    (name "r-osqp")
    (version "0.6.0.8")
    (source (origin
              (method url-fetch)
              (uri (cran-uri "osqp" version))
              (sha256
               (base32
                "15zd0byk8vy899hm7kd0hpx84hnr84ynai29mr7frraamr2l00ql"))))
    (properties `((upstream-name . "osqp")))
    (build-system r-build-system)
    (inputs (list gcc))
    (propagated-inputs (list r-matrix r-r6 r-rcpp))
    (home-page "https://osqp.org")
    (synopsis "Quadratic Programming Solver using the 'OSQP' Library")
    (description
     "This package provides bindings to the OSQP solver.  The OSQP solver is a
numerical optimization package or solving convex quadratic programs written in C
and based on the alternating direction method of multipliers.  See
<arXiv:1711.08013> for details.")
    (license (list asl2.0
                   (fsdg-compatible "file://LICENSE")))))

(define-public r-fdapace
  (package
    (name "r-fdapace")
    (version "0.5.9")
    (source (origin
              (method url-fetch)
              (uri (cran-uri "fdapace" version))
              (sha256
               (base32
                "16j731d7y290xk5qvld59pb78mrch5i61szcf3j79kkirmz6hh8f"))))
    (properties `((upstream-name . "fdapace")))
    (build-system r-build-system)
    (propagated-inputs (list r-hmisc
                             r-mass
                             r-matrix
                             r-numderiv
                             r-pracma
                             r-rcpp
                             r-rcppeigen))
    (native-inputs (list r-knitr))
    (home-page "https://github.com/functionaldata/tPACE")
    (synopsis "Functional Data Analysis and Empirical Dynamics")
    (description
     "This package provides a versatile package that provides implementation of
various methods of Functional Data Analysis (FDA) and Empirical Dynamics.  The
core of this package is Functional Principal Component Analysis (FPCA), a key
technique for functional data analysis, for sparsely or densely sampled random
trajectories and time courses, via the Principal Analysis by Conditional
Estimation (PACE) algorithm.  This core algorithm yields covariance and mean
functions, eigenfunctions and principal component (scores), for both functional
data and derivatives, for both dense (functional) and sparse (longitudinal)
sampling designs.  For sparse designs, it provides fitted continuous
trajectories with confidence bands, even for subjects with very few longitudinal
observations.  PACE is a viable and flexible alternative to random effects
modeling of longitudinal data.  There is also a Matlab version (PACE) that
contains some methods not available on fdapace and vice versa.  Updates to
fdapace were supported by grants from NIH Echo and NSF DMS-1712864 and
DMS-2014626.  Please cite our package if you use it (You may run the command
citation(\"fdapace\") to get the citation format and bibtex entry).  References:
Wang, J.L., Chiou, J., MÃ¼ller, H.G. (2016)
<doi:10.1146/annurev-statistics-041715-033624>; Chen, K., Zhang, X., Petersen,
A., MÃ¼ller, H.G. (2017) <doi:10.1007/s12561-015-9137-5>.")
    (license bsd-3)))

(define-public r-fdadensity
  (package
    (name "r-fdadensity")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (cran-uri "fdadensity" version))
              (sha256
               (base32
                "1i2xzchlsixgjzyfmbv547sy6mq90xrnyc2kpd11wwfgfrpj1ix3"))))
    (properties `((upstream-name . "fdadensity")))
    (build-system r-build-system)
    (propagated-inputs (list r-fdapace r-rcpp))
    (home-page "https://github.com/functionaldata/tDENS")
    (synopsis
     "Functional Data Analysis for Density Functions by Transformation to a Hilbert Space")
    (description
     "An implementation of the methodology described in Petersen and Mueller (2016)
<doi:10.1214/15-AOS1363> for the functional data analysis of samples of density
functions.  Densities are first transformed to their corresponding log quantile
densities, followed by ordinary Functional Principal Components Analysis (FPCA).
 Transformation modes of variation yield improved interpretation of the
variability in the data as compared to FPCA on the densities themselves.  The
standard fraction of variance explained (FVE) criterion commonly used for
functional data is adapted to the transformation setting, also allowing for an
alternative quantification of variability for density data through the
Wasserstein metric of optimal transport.")
    (license bsd-3)))


(define-public r-frechet
  (package
    (name "r-frechet")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (cran-uri "frechet" version))
              (sha256
               (base32
                "1006mgy9avwkwy0666maac59xp4j27wq5yy3pwc3h1r05myxw379"))))
    (properties `((upstream-name . "frechet")))
    (build-system r-build-system)
    (propagated-inputs (list r-corrplot
                             r-fdadensity
                             r-fdapace
                             r-matrix
                             r-osqp
                             r-pracma))
    (home-page "https://github.com/functionaldata/tFrechet")
    (synopsis "Statistical Analysis for Random Objects and Non-Euclidean Data")
    (description
     "This package provides implementation of statistical methods for random objects
lying in various metric spaces, which are not necessarily linear spaces.  The
core of this package is FrÃ©chet regression for random objects with Euclidean
predictors, which allows one to perform regression analysis for non-Euclidean
responses under some mild conditions.  Examples include distributions in
L^2-Wasserstein space, covariance matrices endowed with power metric (with
Frobenius metric as a special case), Cholesky and log-Cholesky metrics.
References: Petersen, A., & MÃ¼ller, H.-G. (2019) <doi:10.1214/17-AOS1624>.")
    (license bsd-3)))
