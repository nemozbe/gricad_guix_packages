(define-module (common gdl)
        #:use-module ((guix licenses) #:prefix license:)
        #:use-module (guix packages)
        #:use-module (guix download)
      	#:use-module (guix utils)
        #:use-module (guix build utils)
        #:use-module (guix build-system cmake)
        #:use-module (guix build-system gnu)
        #:use-module (guix build-system python)
        #:use-module (gnu packages)
        #:use-module (gnu packages pkg-config)
        #:use-module (gnu packages qt)
        #:use-module (gnu packages gl)
        #:use-module (gnu packages c)
        #:use-module (gnu packages readline)
        #:use-module (gnu packages onc-rpc)
        #:use-module (gnu packages image)
        #:use-module (gnu packages imagemagick)
        #:use-module (gnu packages wxwidgets)
        #:use-module (gnu packages geo)
        #:use-module (gnu packages gcc)
        #:use-module (gnu packages xml)
        #:use-module (gnu packages xorg)
        #:use-module (gnu packages compression)
        #:use-module (gnu packages boost)
        #:use-module (gnu packages algebra)
        #:use-module (gnu packages gdb)
        #:use-module (gnu packages libffi)
        #:use-module (gnu packages sqlite)
        #:use-module (gnu packages image-processing)
        #:use-module (gnu packages graphics)
        #:use-module (gnu packages maths)
        #:use-module (gnu packages mpi)
        #:use-module (gnu packages wxwidgets)
        #:use-module (gnu packages ncurses)
        #:use-module (gnu packages python)
        #:use-module (gnu packages python-xyz)
        #:use-module (gnu packages perl)
        #:use-module (gnu packages sphinx)
        #:use-module (gnu packages commencement)
        #:use-module (gnu packages graphviz)
        #:use-module (gnu packages multiprecision))



(define-public libaec
  (package
    (name "libaec")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://gitlab.dkrz.de/k202009/libaec/-/archive/v" version "/libaec-v" version ".tar.gz"))
        (sha256
          (base32
            "0gz9bwzin7n53ywn4gw4bzwf6s7569b0h3cfljmcv0d04imx4x96"
	  )
	)
      )
    )
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags
        (list 
	  (string-append "-DCMAKE_INSTALL_PREFIX="
            (assoc-ref %outputs "out")
	  )
	)
       #:phases
        (modify-phases %standard-phases
          (add-after 'unpack 'post-unpack
            (lambda* (#:key outputs inputs #:allow-other-keys)
	             (mkdir-p "./build")
	             (chdir "./build")
            )
          )
          (replace 'configure
            (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
              (let ((out (assoc-ref outputs "out")))
              (apply invoke "cmake" "../" configure-flags)))))
      #:tests? #f))
    (native-inputs
     `(("gfortran" ,gfortran-toolchain)))
    (home-page "https://github.com/tschoonj/easyrng")
    (synopsis
    "libaec - Adaptive Entropy Coding library.")
    (description
    "Libaec provides fast lossless compression of 1 up to 32 bit wide signed or unsigned integers (samples). The library achieves best results for low entropy data as often encountered in space imaging instrument data or numerical model output from weather or climate simulations. While floating point representations are not directly supported, they can also be efficiently coded by grouping exponents and mantissa.")
    (license license:bsd-2)
  )
)


(define-public eccodes
(package
  (name "eccodes")
  (version "2.26.0")
  (source
    (origin
      (method url-fetch)
      (uri (string-append "https://confluence.ecmwf.int/download/attachments/45757960/eccodes-" version "-Source.tar.gz"))
      (sha256
        (base32 "0j9fnmx3fs63xay0bkhsymb4xxf3rckxdxmqn05qqsp128k66brr"))))
  (build-system cmake-build-system)
  (arguments
   `(#:configure-flags
      (list 
	      (string-append "-DCMAKE_INSTALL_PREFIX="
			                 (assoc-ref %outputs "out")))
     #:phases
      (modify-phases %standard-phases
        (delete 'validate-runpath)		     
        (add-after 'unpack 'post-unpack
          (lambda* (#:key outputs inputs #:allow-other-keys)
                   (mkdir-p "./build")
                   (chdir "./build")
                   (setenv "FC" (which "gfortran"))
          )
	)
        (replace 'configure
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
              (apply invoke "cmake" "../" configure-flags)
	    )
	  )
	)
       )
      #:tests? #f
    )
  )
    (native-inputs
     `(("gfortran" ,gfortran-toolchain)))
  (propagated-inputs
    `(("netcdf" ,netcdf)
      ("hdf4-alt" ,hdf4-alt)
      ("hdf5" ,hdf5)
      ("perl" ,perl)
      ("libaec" ,libaec))
  )
  (synopsis "ecCodes is a package developed by ECMWF which provides an application programming interface and a set of tools for decoding and encoding messages.")
  (description
   "ecCodes is a package developed by ECMWF which provides an application programming interface and a set of tools for decoding and encoding messages in the following formats: WMO FM-92 GRIB edition 1 and edition 2, WMO FM-94 BUFR edition 3 and edition 4, WMO GTS abbreviated header (only decoding). A useful set of command line tools provide quick access to the messages. C, Fortran 90 and Python interfaces provide access to the main ecCodes functionality. ecCodes is an evolution of GRIB-API. It is designed to provide the user with a simple set of functions to access data from several formats with a key/value approach. For GRIB encoding and decoding, the GRIB-API functionality is provided fully in ecCodes with only minor interface and behaviour changes. Interfaces for C, Fortran 90 and Python are all maintained as in GRIB-API. However, the GRIB-API Fortran 77 interface is no longer available. In addition, a new set of functions with the prefix \"codes_\" is provided to operate on all the supported message formats. These functions have the same interface and behaviour as the \"grib_\" functions. A selection of GRIB-API tools has been included in ecCodes (ecCodes GRIB tools), while new tools are available for the BUFR (ecCodes BUFR tools) and GTS formats. The new tools have been developed to be as similar as possible to the existing GRIB-API tools maintaining, where possible, the same options and behaviour. A significant difference compared with GRIB-API tools is that bufr_dump produces output in JSON format suitable for many web based applications.")
  (home-page "http://plplot.sourceforge.net/")
  (license license:gpl2)))


(define-public plplot
(package
  (name "plplot")
  (version "5.15.0")
  (source
    (origin
      (method url-fetch)
      (uri (string-append "https://sourceforge.net/projects/plplot/files/plplot/" version "%20Source/plplot-" version ".tar.gz"))
      (sha256
        (base32 "0ywccb6bs1389zjfmc9zwdvdsvlpm7vg957whh6b5a96yvcf8bdr"))))
  (build-system cmake-build-system)
  (arguments
   `(#:configure-flags
      (list 
	      (string-append "-DCMAKE_INSTALL_PREFIX="
			                 (assoc-ref %outputs "out")))
     #:phases
      (modify-phases %standard-phases
        (add-after 'unpack 'post-unpack
          (lambda* (#:key outputs inputs #:allow-other-keys)
	          (mkdir-p "./build")
	          (chdir "./build")))
        (replace 'configure
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
              (apply invoke "cmake" "../" configure-flags)))))
      #:tests? #f))
  (propagated-inputs
    `(("wxwidgets" ,wxwidgets)
      ("netcdf" ,netcdf)
      ("hdf4-alt" ,hdf4-alt)
      ("hdf5" ,hdf5)
      ("fftw" ,fftw)
      ("fftwf" ,fftwf)
      ("fftw-openmpi" ,fftw-openmpi)
      ("eccodes" ,eccodes)
      ("libjpeg-turbo" ,libjpeg-turbo)
      ("libgeotiff" ,libgeotiff)
      ("libtiff" ,libtiff))
  )
  (synopsis "Cross-platform, scientific graphics plotting library.")
  (description
   "PLplot is a cross-platform, scientific graphics plotting library that supports math symbols and human languages (via UTF-8 user input strings); plot capabilities for multiple non-interactive plot file formats and in multiple interactive environments; and bindings for multiple computer languages.")
  (home-page "http://plplot.sourceforge.net/")
  (license license:gpl2)))


(define-public gdl 
  (package
  (name "gdl")
  (version "v1.0.1")
  (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/gnudatalanguage/gdl/archive/refs/tags/" version ".tar.gz"))
            (sha256
             (base32
              "0jj7c35fn5wy91sf7fkivrjkcd5kz93x7hlmy5ghgd4nq2zml1gb"))))
  (build-system cmake-build-system)
  (arguments
   `(#:configure-flags
      (list
	      (string-append "-DX11=ON")
	      (string-append "-DCMAKE_INSTALL_PREFIX="
			                 (assoc-ref %outputs "out")))
     #:phases
      (modify-phases %standard-phases
        (add-after 'unpack 'post-unpack
          (lambda* (#:key outputs inputs #:allow-other-keys)
	          (mkdir-p "./build")
	          (chdir "./build")))
        (replace 'configure
          (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
              (apply invoke "cmake" "../" configure-flags)))))
      #:tests? #f))
  (propagated-inputs
    `(("gdb" ,gdb)
      ("ncurses" ,ncurses)
      ("zlib" ,zlib)
      ("gsl" ,gsl)
      ("graphicsmagick" ,graphicsmagick)
      ("wxwidgets" ,wxwidgets)
      ("netcdf" ,netcdf)
      ("hdf4-alt" ,hdf4-alt)
      ("hdf5" ,hdf5)
      ("fftw" ,fftw)
      ("proj" ,proj)
      ("shapelib" ,shapelib)
      ("expat" ,expat)
      ("mpich" ,mpich)
      ("python" ,python)
      ("python-numpy" ,python-numpy)
      ("udunits" ,udunits)
      ("eigen" ,eigen)
      ("glpk" ,glpk)
      ("readline" ,readline)
      ("libpng" ,libpng)
      ("libtirpc" ,libtirpc)
      ("plplot" ,plplot)
      ("eccodes" ,eccodes)
      ("openmpi" ,openmpi))
  )
  (inputs
    `(("boost" ,boost)
      ("pkg-config" ,pkg-config)
      ("openblas" ,openblas)
      ("libx11" ,libx11)
      ("gcc-toolchain" ,gcc-toolchain))
  )

  (synopsis "GDL is a free/libre/open source incremental compiler compatible with IDL (Interactive Data Language) and to some extent with PV-WAVE.")
  (description
   "GDL is a domain-specific programming language and a data analysis environment. As a language, it is dynamically-typed, array-oriented, vectorised and has object-oriented programming capabilities. GDL library routines handle numerical calculations, data visualisation, signal/image processing, interaction with host OS and data input/output. GDL supports several data formats such as netCDF, HDF4, HDF5, GRIB, PNG, TIFF, DICOM, etc. Graphical output is handled by X11, PostScript, SVG or z-buffer terminals, the last one allowing output graphics (plots) to be saved in a variety of raster graphics formats. GDL features integrated debugging facilities. The built-in widget functionality enables development of GUI-based software. GDL has also a Python bridge (Python code can be called from GDL; GDL can be compiled as a Python module). Development and maintenance of GDL is carried out targeting Linux, BSD, OSX and Windows (MinGW, Cygwin).")
  (home-page "https://gnudatalanguage.github.io/")
  (license license:gpl3+)))