(define-module (common absuite)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages base)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages python)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages check)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages machine-learning)
  #:use-module (gnu packages bootstrap)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages time)
  #:use-module (gnu packages libidn)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages geo))

(define-public python-hdmedians
  (package
    (name "python-hdmedians")
    (version "0.14.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "hdmedians" version))
        (sha256
          (base32
            "1mn2k8srnmfy451l7zvb2l4hn9701bc5awjm6q3vmqbicyqyqyml"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-cython" ,python-cython)
        ("python-nose" ,python-nose)
        ("python-numpy" ,python-numpy)))
    (home-page
      "http://github.com/daleroberts/hdmedians")
    (synopsis "High-dimensional medians")
    (description "High-dimensional medians")
    (license license:asl2.0)))

(define-public python-scikit-bio
  (package
    (name "python-scikit-bio")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "scikit-bio" version))
        (sha256
          (base32
            "1sph6yf23d0pwd5dvsfkm0jiaz2jjg7wzwq57qp2rzz07k2kxds8"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f)) 
   (propagated-inputs
      `(("python-cachecontrol" ,python-cachecontrol)
        ("python-decorator" ,python-decorator)
        ("python-hdmedians" ,python-hdmedians)
        ("python-ipython-custom" ,python-ipython-custom)
        ("python-lockfile" ,python-lockfile)
        ("python-matplotlib" ,python-matplotlib)
        ("python-natsort" ,python-natsort)
        ("python-numpy" ,python-numpy)
        ("python-pandas" ,python-pandas)
        ("python-scikit-learn" ,python-scikit-learn)
        ("python-scipy" ,python-scipy)))
    (home-page "http://scikit-bio.org")
    (synopsis
      "Data structures, algorithms and educational resources for bioinformatics.")
    (description
      "Data structures, algorithms and educational resources for bioinformatics.")
    (license #f)))

(define-public python-nwalign3
  (let ((branch "master")
        (commit "0ebd3a7fee73c4041acebe5575cffce49a3fdeae"))
  (package
    (name "python-nwalign3")
    (version (git-version "1" branch commit))
    (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/briney/nwalign3")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "0p6df4y1d7s7w0vj48ys2hrw5k5ff5amvjq9xcc0ya379yb5l748"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-cython" ,python-cython)
        ("python-numpy" ,python-numpy)))
    (home-page "https://github.com/briney/nwalign3")
    (synopsis
      "Needleman-Wunsch global sequence alignment")
    (description
      "Needleman-Wunsch global sequence alignment")
    (license license:bsd-3))))


(define-public python-clonify
  (let ((branch "development")
        (commit "79f42cb89dc2fcfb85c07e78404caf8d749edf13"))
  (package
    (name "python-clonify")
    (version (git-version "1" branch commit))
    (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/briney/clonify")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "1z8rg7h8h9yfsmldxf0yqwk9vjq4xfklm99v49b8lqvs6l9vmv9r"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-cython" ,python-cython)
	      ("python-abstar" ,python-abstar)
        ("python-nose" ,python-nose)
        ("python-numpy" ,python-numpy)))
    (home-page
      "https://github.com/briney/clonify")
    (synopsis "Utils working with abstar")
    (description "Utils working with abstar")
    (license license:asl2.0))))

(define-public python-pytest-rerunfailures
  (package
    (name "python-pytest-rerunfailures")
    (version "10.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pytest-rerunfailures" version))
        (sha256
          (base32
            "0ws2hbgh00nd6xchyi9ymyxfpg5jpxsy5mxdz4nxvriyw5nw05vn"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-pytest" ,python-pytest)
        ("python-setuptools" ,python-setuptools)))
    (home-page
      "https://github.com/pytest-dev/pytest-rerunfailures")
    (synopsis
      "pytest plugin to re-run tests to eliminate flaky failures")
    (description
      "pytest plugin to re-run tests to eliminate flaky failures")
    (license #f)))

(define-public python-parameterizedtestcase
  (package
    (name "python-parameterizedtestcase")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "parameterizedtestcase" version))
        (sha256
          (base32
            "0zhjmsd16xacg4vd7zb75kw8q9khn52wvad634v1bvz7swaivk2c"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (home-page
      "https://github.com/msabramo/python_unittest_parameterized_test_case")
    (synopsis
      "Parameterized tests for Python's unittest module")
    (description
      "Parameterized tests for Python's unittest module")
    (license license:expat)))

(define-public python-azure-common
  (package
    (name "python-azure-common")
    (version "1.1.27")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "azure-common" version ".zip"))
        (sha256
          (base32
            "1vs1mmxmrzy8k6gccdp2szg76s9whr73rx8c0n9vvb1322cmsgwz"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-azure-nspkg" ,python-azure-nspkg)))
    (native-inputs `(("unzip" ,unzip)))
    (home-page
      "https://github.com/Azure/azure-sdk-for-python")
    (synopsis
      "Microsoft Azure Client Library for Python (Common)")
    (description
      "Microsoft Azure Client Library for Python (Common)")
    (license license:expat)))

(define-public python-msrest
  (package
    (name "python-msrest")
    (version "0.6.21")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "msrest" version))
        (sha256
          (base32
            "1n389m3hcsyjskzimq4j71nyw9pjkrp0n5wg1q2c4bfwpv3inrkj"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-certifi" ,python-certifi)
       ;; ("python-enum34" ,python-enum34)
        ("python-isodate" ,python-isodate)
        ("python-requests" ,python-requests)
        ("python-requests-oauthlib"
         ,python-requests-oauthlib)
        ("python-typing-extensions" ,python-typing-extensions)))
    (home-page
      "https://github.com/Azure/msrest-for-python")
    (synopsis
      "AutoRest swagger generator Python client runtime.")
    (description
      "AutoRest swagger generator Python client runtime.")
    (license license:expat)))

(define-public python-azure-core
  (package
    (name "python-azure-core")
    (version "1.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "azure-core" version ".zip"))
        (sha256
          (base32
            "0ijg6d51qwgc5l48smdgl3vvgafxrqcbny6g3vjd6hp1vn876h15"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-azure-nspkg" ,python-azure-nspkg)
        ;;("python-enum34" ,python-enum34)
        ("python-requests" ,python-requests)
        ("python-six" ,python-six)
        ("python-typing-extensions" ,python-typing-extensions)))
    (native-inputs `(("unzip" ,unzip)))
    (home-page
      "https://github.com/Azure/azure-sdk-for-python/tree/main/sdk/core/azure-core")
    (synopsis
      "Microsoft Azure Core Library for Python")
    (description
      "Microsoft Azure Core Library for Python")
    (license license:expat)))

(define-public python-azure-storage-blob
  (package
    (name "python-azure-storage-blob")
    (version "12.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "azure-storage-blob" version ".zip"))
        (sha256
          (base32
            "00b47kwkl6i6wwj4dgvhc5fya535csq0733ckyr5irdnvw6vadzb"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-azure-core" ,python-azure-core)
        ("python-azure-storage-nspkg"
         ,python-azure-storage-nspkg)
        ("python-cryptography" ,python-cryptography)
        ;;("python-enum34" ,python-enum34)
        ;;("python-futures" ,python-futures)
        ("python-msrest" ,python-msrest)
        ("python-typing-extensions" ,python-typing-extensions)))
    (native-inputs `(("unzip" ,unzip)))
    (home-page
      "https://github.com/Azure/azure-sdk-for-python/tree/master/sdk/storage/azure-storage-blob")
    (synopsis
      "Microsoft Azure Blob Storage Client Library for Python")
    (description
      "Microsoft Azure Blob Storage Client Library for Python")
    (license license:expat)))

(define-public python-google-crc32c
  (package
    (name "python-google-crc32c")
    (version "1.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-crc32c" version))
        (sha256
          (base32
            "09qbsvmbdhd8mlq8sy67pa758i41lzkmvllr16anczvk6q9bvxfz"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-cffi" ,python-cffi)))
    (native-inputs
      `(("python-pytest" ,python-pytest)))
    (home-page
      "https://github.com/googleapis/python-crc32c")
    (synopsis
      "A python wrapper of the C library 'Google CRC32C'")
    (description
      "A python wrapper of the C library 'Google CRC32C'")
    (license license:asl2.0)))

(define-public python-google-resumable-media
  (package
    (name "python-google-resumable-media")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-resumable-media" version))
        (sha256
          (base32
            "0rr1kw189296s516692dj2x7z6r3plrkisl3j29sqja6ff0h6k09"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-google-crc32c" ,python-google-crc32c)))
    (home-page
      "https://github.com/googleapis/google-resumable-media-python")
    (synopsis
      "Utilities for Google Media Downloads and Resumable Uploads")
    (description
      "Utilities for Google Media Downloads and Resumable Uploads")
    (license license:asl2.0)))

(define-public python-google-cloud-core
  (package
    (name "python-google-cloud-core")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-cloud-core" version))
        (sha256
          (base32
            "00i75ik1ivr4ika3girrniigcn1ys5cgr9w1cyhi37ngiij9kvlh"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-google-api-core"
         ,python-google-api-core)
        ("python-google-auth" ,python-google-auth)))
    (home-page
      "https://github.com/googleapis/python-cloud-core")
    (synopsis "Google Cloud API client core library")
    (description
      "Google Cloud API client core library")
    (license license:asl2.0)))

(define-public python-futures
  (package
    (name "python-futures")
    (version "3.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "futures" version))
        (sha256
          (base32
            "154pvaybk9ncyb1wpcnzgd7ayvvhhzk92ynsas7gadaydbvkl0vy"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (home-page
      "https://github.com/agronholm/pythonfutures")
    (synopsis
      "Backport of the concurrent.futures package from Python 3")
    (description
      "Backport of the concurrent.futures package from Python 3")
    (license #f)))

(define-public python-google-api-core
  (package
    (name "python-google-api-core")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-api-core" version))
        (sha256
          (base32
            "13bndl9xlh7g01pbwn624r2462qri9wd0bxkvmpxs42fkxqb17mx"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(;;("python-futures" ,python-futures)
        ("python-google-auth" ,python-google-auth)
        ("python-googleapis-common-protos"
         ,python-googleapis-common-protos)
        ("python-protobuf" ,python-protobuf)
        ("python-requests" ,python-requests)
        ("python-setuptools" ,python-setuptools)))
    (home-page
      "https://github.com/googleapis/python-api-core")
    (synopsis "Google API client core library")
    (description "Google API client core library")
    (license license:asl2.0)))

(define-public python-google-auth
  (package
    (name "python-google-auth")
    (version "2.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-auth" version))
        (sha256
          (base32
            "1lvv83qn015ryqay04vwx307nc0763909xvh8hzbfvp0nd8g06pa"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-cachetools" ,python-cachetools)
        ("python-pyasn1-modules" ,python-pyasn1-modules)
        ("python-rsa" ,python-rsa)
        ("python-setuptools" ,python-setuptools)))
    (home-page
      "https://github.com/googleapis/google-auth-library-python")
    (synopsis "Google Authentication Library")
    (description "Google Authentication Library")
    (license license:asl2.0)))

(define-public python-googleapis-common-protos
  (package
    (name "python-googleapis-common-protos")
    (version "1.53.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "googleapis-common-protos" version))
        (sha256
          (base32
            "1x7bahcgnj4hnjb096s30ryad2iw5pv5qbgc7in1za507a8fi3m8"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-protobuf" ,python-protobuf)))
    (home-page
      "https://github.com/googleapis/python-api-common-protos")
    (synopsis "Common protobufs used in Google APIs")
    (description
      "Common protobufs used in Google APIs")
    (license #f)))

(define-public python-google-cloud-storage
  (package
    (name "python-google-cloud-storage")
    (version "1.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-cloud-storage" version))
        (sha256
          (base32
            "1bmx80c7p1dnsgm6hsphk2wq4zc6brad8gc0qr7g5p4f344kvpf1"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-google-api-core"
         ,python-google-api-core)
        ("python-google-auth" ,python-google-auth)
        ("python-google-cloud-core"
         ,python-google-cloud-core)
        ("python-google-resumable-media"
         ,python-google-resumable-media)
        ("python-googleapis-common-protos"
         ,python-googleapis-common-protos)
        ("python-requests" ,python-requests)))
    (home-page
      "https://github.com/googleapis/python-storage")
    (synopsis
      "Google Cloud Storage API client library")
    (description
      "Google Cloud Storage API client library")
    (license license:asl2.0)))

(define-public python-smart-open
  (package
    (name "python-smart-open")
    (version "5.2.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://files.pythonhosted.org/packages/6e/80/287bd6bfdc4e251500fa12637e28b3f94ac14fd96cec9ca8ce284676ce80/smart_open-5.2.0.tar.gz")
        (sha256
          (base32
            "0i8jiksb2yvqzj8d3kg15bfwlskqs604wdbwfabpm30ss2g473rh"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (native-inputs
      `(("python-azure-common" ,python-azure-common)
        ("python-azure-core" ,python-azure-core)
        ("python-azure-storage-blob"
         ,python-azure-storage-blob)
        ("python-boto3" ,python-boto3)
        ("python-google-cloud-storage"
         ,python-google-cloud-storage)
        ;;("python-moto" ,python-moto)
        ; ("python-parameterizedtestcase"
        ;  ,python-parameterizedtestcase)
        ("python-paramiko" ,python-paramiko)
        ("python-pathlib2" ,python-pathlib2)
        ("python-pytest" ,python-pytest)
        ("python-pytest-rerunfailures"
         ,python-pytest-rerunfailures)
        ("python-requests" ,python-requests)
        ("python-responses" ,python-responses)))
    (home-page
      "https://github.com/piskvorky/smart_open")
    (synopsis
      "Utils for streaming large files (S3, HDFS, GCS, Azure Blob Storage, gzip, bz2...)")
    (description
      "Utils for streaming large files (S3, HDFS, GCS, Azure Blob Storage, gzip, bz2...)")
    (license license:expat)))

(define-public python-hdmedians
  (package
    (name "python-hdmedians")
    (version "0.14.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "hdmedians" version))
        (sha256
          (base32
            "1mn2k8srnmfy451l7zvb2l4hn9701bc5awjm6q3vmqbicyqyqyml"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-cython" ,python-cython)
	("python-nose" ,python-nose)
        ("python-numpy" ,python-numpy)))
    (home-page
      "http://github.com/daleroberts/hdmedians")
    (synopsis "High-dimensional medians")
    (description "High-dimensional medians")
    (license license:asl2.0)))

(define-public python-sample-sheet
  (package
    (name "python-sample-sheet")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "sample-sheet" version))
        (sha256
          (base32
            "1wrbnbhp8vb0wnhbk4k0fjcdghxzpfdlccllbxxk4abgzdmw9kkg"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
      `(("python-click" ,python-click)
        ("python-requests" ,python-requests)
        ("python-tabulate" ,python-tabulate)
        ("python-terminaltables" ,python-terminaltables)))
    (home-page
      "https://github.com/clintval/sample-sheet")
    (synopsis
      "An Illumina Sample Sheet parsing library")
    (description
      "An Illumina Sample Sheet parsing library")
    (license license:expat)))

(define-public python-ete3
  (package
    (name "python-ete3")
    (version "3.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "ete3" version))
        (sha256
          (base32
            "1sariq57v77adbg8h9vj8aymrjsm2hgi1cgs11k9v219qnw8gjag"))))
    (build-system python-build-system)
    (arguments
        `(#:tests? #f
          #:phases
            (modify-phases %standard-phases
              (delete 'sanity-check))))
    (propagated-inputs (list python-six python-numpy python-lxml))
    (home-page "http://etetoolkit.org")
    (synopsis
      "A Python Environment for (phylogenetic) Tree Exploration")
    (description
      "A Python Environment for (phylogenetic) Tree Exploration")
    (license #f)))

(define-public python-ipython-custom
  (package
    (name "python-ipython-custom")
    (version "7.27.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ipython" version ".tar.gz"))
       (sha256
        (base32 "04xgymypnbfgf2q0d5b0hanjbjsp53f055sh1p8xlq52vyzmxdaq"))))
    (build-system python-build-system)
    (propagated-inputs
     (list python-backcall
           python-pyzmq
           python-prompt-toolkit
           python-terminado
           python-matplotlib
           python-matplotlib-inline
           python-numpy
           python-numpydoc
           python-jedi
           python-jinja2
           python-mistune
           python-pexpect
           python-pickleshare
           python-simplegeneric
           python-jsonschema
           python-traitlets
           python-nbformat
           python-pygments))
    (inputs
     (list readline which))
    (native-inputs
     (list graphviz
           pkg-config
           python-requests ;; for tests
           python-testpath
           python-nose))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'make-docs-reproducible
           (lambda _
             (substitute* "IPython/sphinxext/ipython_directive.py"
               ((".*import datetime") "")
               ((".*datetime.datetime.now\\(\\)") "")
               (("%timeit") "# %timeit"))))
         ;; Tests can only be run after the library has been installed and not
         ;; within the source directory.
         (delete 'check)
         (add-after 'install 'check
           (lambda* (#:key inputs outputs tests? #:allow-other-keys)
             (if tests?
                 (begin
                   ;; Make installed package available for running the tests
                   (add-installed-pythonpath inputs outputs)
                   (setenv "HOME" "/tmp/") ;; required by a test
                   ;; We only test the core because one of the other tests
                   ;; tries to import ipykernel.
                   (invoke "python" "IPython/testing/iptest.py"
                           "-v" "IPython/core/tests")))))
         (add-before 'check 'fix-tests
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "./IPython/utils/_process_posix.py"
               (("/usr/bin/env', 'which") (which "which")))
             (substitute* "./IPython/core/tests/test_inputtransformer.py"
               (("#!/usr/bin/env python")
                (string-append "#!" (which "python"))))
             ;; This test introduces a circular dependency on ipykernel
             ;; (which depends on ipython).
             (delete-file "IPython/core/tests/test_display.py")
             ;; AttributeError: module 'IPython.core' has no attribute 'formatters'
             (delete-file "IPython/core/tests/test_interactiveshell.py")
             ;; AttributeError: module 'matplotlib_inline' has no
             ;; attribute 'backend_inline'
             (delete-file "IPython/core/tests/test_pylabtools.py"))))))
    (home-page "https://ipython.org")
    (synopsis "IPython is a tool for interactive computing in Python")
    (description
     "IPython provides a rich architecture for interactive computing with:
Powerful interactive shells, a browser-based notebook, support for interactive
data visualization, embeddable interpreters and tools for parallel
computing.")
    (properties '((cpe-name . "ipython")))
    (license license:bsd-3)))

(define-public python-abutils
  (let ((branch "development")
        (commit "cb0ebe16182094118934412274e225969e608623"))
  (package
    (name "python-abutils")
    (version (git-version "1" branch commit))
    (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/briney/abutils")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "09jn74w4535jijp2kff8fkg5vix16z01yf5vkb2b45idmbkbscmw"))))
    (build-system python-build-system)
    (arguments 
      '(#:tests? #f
        #:phases
       	  (modify-phases %standard-phases
            (delete 'validate-runpath)
            (delete 'sanity-check))))
    (propagated-inputs
      `(("python-celery" ,python-celery)
        ("python-ete3" ,python-ete3)
	      ("gcc-toolchain" ,gcc-toolchain)
        ("python-matplotlib" ,python-matplotlib)
        ("python-natsort" ,python-natsort)
        ("python-numpy" ,python-numpy)
        ("python-nwalign3" ,python-nwalign3)
        ("python-pandas" ,python-pandas)
        ("python-paramiko" ,python-paramiko)
        ("python-pymongo" ,python-pymongo)
        ("python-pytest" ,python-pytest)
        ("python-pyyaml" ,python-pyyaml)
        ("python-sample-sheet" ,python-sample-sheet)
        ("python-scikit-bio" ,python-scikit-bio)
        ("python-scikit-learn" ,python-scikit-learn)
        ("python-scipy" ,python-scipy)
        ("python-seaborn" ,python-seaborn)
        ("python-smart-open" ,python-smart-open)))
    (home-page
      "https://www.github.com/briney/abutils")
    (synopsis
      "Utilities for analysis of antibody NGS data")
    (description
      "Utilities for analysis of antibody NGS data")
    (license #f))))

(define-public python-abtools
  (let ((branch "development")
        (commit "4bade5dfe4e1aa1de787bad6307f06093418c30d"))
  (package
    (name "python-abtools")
    (version (git-version "1" branch commit))
    (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/briney/abtools")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "1m6kcipqyzb2sh77cgyfnnaiqiv02dj0nay1jilwxnjin3i8xr9i"))))
    (build-system python-build-system)
    (arguments 
      '(#:tests? #f
        #:phases
       	  (modify-phases %standard-phases
            (delete 'sanity-check))))
    (propagated-inputs
      `(("python-biopython" ,python-biopython)
        ("python-celery" ,python-celery)
        ("python-abutils" ,python-abutils)
        ("python-ete3" ,python-ete3)
        ("python-matplotlib" ,python-matplotlib)
        ("python-nose" ,python-nose)
        ("python-numpy" ,python-numpy)
        ("python-nwalign3" ,python-nwalign3)
        ("python-pandas" ,python-pandas)
        ("python-pymongo" ,python-pymongo)
        ("python-scikit-bio" ,python-scikit-bio)
        ("python-seaborn" ,python-seaborn)))
    (home-page
      "https://www.github.com/briney/abtools")
    (synopsis
      "Utilities for analysis of antibody NGS data")
    (description
      "Utilities for analysis of antibody NGS data")
    (license #f))))

(define-public python-abstar
  (let ((branch "master")
        (commit "3ec5fec4c6c766be570f33e379e32e584b46e738"))
  (package
    (name "python-abstar")
    (version (git-version "1" branch commit))
    (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/briney/abstar")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "0d4khpis08iiw1qlpbjli6j3spfxxys91xjvh8g0mxhk5klh8avc"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:phases
        (modify-phases %standard-phases
	        (delete 'validate-runpath)
	        (delete 'strip)
          (delete 'sanity-check)
	          (add-after 'install 'correct-runpath
              (lambda* (#:key outputs inputs #:allow-other-keys)
	              (define out (assoc-ref outputs "out"))
		            (define blastn 
		        	    (string-append out 
                    "/lib/python3.9/site-packages/abstar/assigners/bin/blastn_linux"))
		              (define makeblast
		        	       (string-append out 
                       "/lib/python3.9/site-packages/abstar/assigners/bin/makeblastdb_linux"))
		              (define zlib (assoc-ref inputs "zlib"))
		              (define bzip2 (assoc-ref inputs "bzip2"))
		              (define libidn (assoc-ref inputs "libidn")) 
		              (define gcc (assoc-ref inputs "gcc:lib"))
		              (define glibc (assoc-ref inputs "glibc"))
		              (define ld.so (string-append glibc ,(glibc-dynamic-linker)))
		              (define runpath_blast (string-append  zlib "/lib:"
		        	    		    gcc "/lib:"
		        	    		    glibc "/lib:"
		        	    		    bzip2 "/lib"))
		              (define runpath_make (string-append zlib "/lib:"
		        	    		    gcc "/lib:"
		        	    		    glibc "/lib:"
		        	    		    bzip2 "/lib:"
		        	    		    libidn "/lib")) 
                  ;; Change runpath without --force-runpath patchelf option!
		              (define (patch-elf ld rpath file)
                                     (make-file-writable file)
                                     (format #t "Setting interpreter on '~a'...~%" file)
                                     (format #t "Interpreter is '~a'...~%" ld)
                                     (invoke "patchelf" "--set-interpreter" ld file)
                                     (format #t "Setting RPATH on '~a'...~%" file)
                                     (format #t "runpath is '~a'...~%" rpath)
                                     (invoke "patchelf" "--set-rpath" rpath  file))
		              (patch-elf ld.so runpath_make makeblast)
		              (patch-elf ld.so runpath_blast blastn)#t)))))	 
   (inputs
     `(("patchelf" ,patchelf)
       ("zlib" ,zlib)
       ("bzip2" ,bzip2)
       ("libidn" ,libidn)
       ("gcc:lib" ,gcc "lib")
       ("glibc" ,glibc)))
   (propagated-inputs
      `(("python-biopython" ,python-biopython)
        ("python-celery" ,python-celery)
        ("python-dask" ,python-dask)
        ("python-numpy" ,python-numpy)
        ("python-nwalign3" ,python-nwalign3)
        ("python-pandas" ,python-pandas)
        ("python-pyarrow" ,python-pyarrow)
        ("python-pymongo" ,python-pymongo)
        ("python-scikit-bio" ,python-scikit-bio)))
    (home-page
      "https://www.github.com/briney/abstar")
    (synopsis
      "VDJ assignment and antibody sequence annotation. Scalable from a single sequence to billions of sequences.")
    (description
      "VDJ assignment and antibody sequence annotation. Scalable from a single sequence to billions of sequences.")
    (license #f))))
