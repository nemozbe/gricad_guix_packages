(define-module (common specfem)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gcc)
  #:use-module (guix gexp)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages package-management))

(define-public specfem3d-mpi
    (package
        (name "specfem3d-mpi")
        (version "4.0.0")
        (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/SPECFEM/specfem3d/archive/refs/tags/v" version ".tar.gz"))
            (sha256
                (base32
                    "1iz58ajl6dw2ibsr3la0fmxk2nlw1p1ynqhsad424grr53rwxasl"))))
        (build-system gnu-build-system)
        (arguments
          `(#:configure-flags
             (list
               (string-append "--prefix=" (assoc-ref %outputs "out"))
	           (string-append "--with-scotch-dir=" (assoc-ref %build-inputs "scotch"))
	           "--with-mpi"
               "MPIFC=mpif90"
               "CC=gcc"
               "FC=gfortran")
            #:phases (modify-phases %standard-phases
              (replace 'install
                (lambda* (#:key outputs #:allow-other-keys)
                  (let* ((out (assoc-ref outputs "out"))
                         (bindir (string-append out "/bin"))
                         (libdir (string-append out "/lib")))
                    (copy-recursively "./bin"
                                 bindir)
                    (copy-recursively "./lib"
                                 libdir)
          ))))))
        (inputs (list zlib
                      openmpi
                      scotch
                      gfortran-toolchain))
        (home-page "https://github.com/SPECFEM/specfem3d")
        (synopsis "SPECFEM3D_Cartesian simulates acoustic (fluid), elastic (solid), coupled acoustic/elastic, 
                   poroelastic or seismic wave propagation 
                   in any type of conforming mesh of hexahedra (structured or not.)")
        (description "SPECFEM3D_Cartesian simulates acoustic (fluid), elastic (solid), coupled acoustic/elastic, poroelastic or seismic wave propagation 
                      in any type of conforming mesh of hexahedra (structured or not.) 
                      It can, for instance, model seismic waves propagating in sedimentary basins or any other regional 
                      geological model following earthquakes. It can also be used for non-destructive testing or for ocean acoustics.
                      SPECFEM3D was founded by Dimitri Komatitsch and Jeroen Tromp 
                      and is now being developed by a large, collaborative, and inclusive community.")
        (license license:gpl3)))